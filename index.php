<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/protected/vendor/yiisoft/yii/framework/yii.php';
//$yii = dirname(__FILE__).'/../yiiframework/YiiBase.php';
$config=dirname(__FILE__).'/protected/config/main.php';
$serverConfig = dirname(__FILE__).'/protected/config/server.php';
include_once($serverConfig);

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',false || (array_key_exists('explain', $_GET) && $_GET['explain'] === 'Y')
    || (array_key_exists('debug', $_GET) && $_GET['debug'] === 'Y'));
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
//execution time
defined('YII_DEBUG_PROFILING') or define('YII_DEBUG_PROFILING',false || YII_DEBUG);
// show extended profiler from Yii (on the WebPage)
defined('YII_DEBUG_PROFILING_EXT') or define('YII_DEBUG_PROFILING_EXT',false || YII_DEBUG_PROFILING);
// default cache time
defined('CACHE_TIME') or define('CACHE_TIME',array_key_exists('clear_cache', $_GET) && $_GET['clear_cache'] === 'Y' ? 0 : 1000);

require_once($yii);

//class Yii extends YiiBase {
//    /**
//     * @static
//     * @return CWebApplication
//     */
//    public static function app() {
//        return parent::app();
//    }
//}

function logDetailed($message, $level, $category, $vars) {
    $m = $message;
    if(is_array($vars)) {
        foreach($vars as $key=>$value) {
            $m .= "</br></br>\n\n$key: " . print_r($value, true);
        }
    }
    Yii::log($m, $level, $category);
}

Yii::createWebApplication($config)->run();

if(YII_DEBUG_PROFILING
    && Yii::app()->getModule('user')->isAdmin()
    && !strpos(Yii::app()->request->url, 'serve')) {
    list($queryCount, $queryTime) = Yii::app()->db->getStats();
    echo "Query count: $queryCount, Total query time: ".sprintf('%0.5f',$queryTime)."s<br/>\n";

    echo 'Page execution time: ' . sprintf('%0.5f',Yii::getLogger()->getExecutionTime()) . 's';
}
