<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 12.01.13
 * Time: 11:24
 * To change this template use File | Settings | File Templates.
 */
class COutputCacheCustom extends COutputCache
{
    protected function checkContentCache()
    {
        if (array_key_exists('clear_cache', $_GET) && $_GET['clear_cache'] === "Y" && Yii::app()->getModule('user')->isAdmin()) {
            $this->getCache()->delete($this->getCacheKey());
            //Yii::app()->cache->delete($this->getCacheKey());
            return false;
        }
        return parent::checkContentCache();
    }


}
