<?php
$assetUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ext.LikeButtonsWidget.assets'));
Yii::app()->clientScript->registerCssFile($assetUrl . '/social-likes.css');
Yii::app()->clientScript->registerScriptFile($assetUrl . '/social-likes.min.js');?>

<ul class="social-likes">
    <li class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</li>
    <li class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</li>
    <li class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</li>
    <li class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</li>
    <li class="plusone" title="Поделиться ссылкой в Гугл-плюсе">Google+</li>
</ul>