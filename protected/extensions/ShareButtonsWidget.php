<?php

class ShareButtonsWidget extends CWidget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $this->render('shareButtons');
    }


}
