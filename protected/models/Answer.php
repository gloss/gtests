<?php

/**
 * This is the model class for table "{{answer}}".
 *
 * The followings are the available columns in table '{{answer}}':
 * @property integer $id
 * @property integer $question_id
 * @property string $text
 * @property integer $correct
 * @property string base_code
 * @property integer $order
 *
 * The followings are the available model relations:
 * @property Question $question
 * @property UserAnswer[] $userAnswers
 */
class Answer extends CActiveRecordTimed
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Answer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{answer}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('question_id, text', 'required'),
			array('question_id, correct, order', 'numerical', 'integerOnly'=>true),
			array('text, base_code', 'length', 'max'=>255),
			array('base_code, order', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, question_id, text, correct', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'question' => array(self::BELONGS_TO, 'Question', 'question_id'),
			'userAnswers' => array(self::HAS_MANY, 'UserAnswer', 'answer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'question_id' => 'Question',
			'text' => 'Text',
			'correct' => 'Correct',
			'base_code' => 'Code of 1C Base',
            'order' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('question_id',$this->question_id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('correct',$this->correct);
        $criteria->compare('order', $this->order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getText()
    {
        return htmlspecialchars($this->text);
    }

//    public function __get($name)
//    {
//        if ($name == "text") {
//            return htmlspecialchars($this->getAttribute($name));
//        }
//        return parent::__get($name);
//    }

	/**
	 * @param mixed  $pk
	 * @param string $condition
	 * @param array  $params
	 *
	 * @return Answer
	 */
	public function findByPk($pk, $condition = '', $params = array())
	{
		return parent::findByPk($pk, $condition, $params);
	}

	public function setAttribute($name, $value)
	{
		$result = parent::setAttribute($name, $value);
		if ($name === "question" && get_class($value) === "Question") {
			$this->question_id = $value->id;
		}
		return $result;
	}

}
