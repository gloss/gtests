<?php

/**
 * This is the model class for table "{{user_attempt}}".
 *
 * The followings are the available columns in table '{{user_attempt}}':
 *
 * @property integer      $id
 * @property string       $start
 * @property string       $end
 * @property integer      $user_id
 * @property integer      $question_id
 * @property integer      $disk_id
 *
 * The followings are the available model relations:
 * @property UserAnswer[] $userAnswers
 * @property Disk         $disk
 * @property Question     $question
 * @property User         $user
 */
class UserAttempt extends UseredModel
{
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return UserAttempt the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user_attempt}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('start, user_id, question_id, disk_id', 'required'),
            array('user_id, question_id, disk_id', 'numerical', 'integerOnly' => true),
            array('end', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, start, end, user_id, question_id, disk_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'userAnswers' => array(self::HAS_MANY, 'UserAnswer', 'attempt_id'),
            'disk' => array(self::BELONGS_TO, 'Disk', 'disk_id'),
            'question' => array(self::BELONGS_TO, 'Question', 'question_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'start' => 'Start',
            'end' => 'End',
            'user_id' => 'User',
            'question_id' => 'Question',
            'disk_id' => 'Disk',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('start', $this->start, true);
        $criteria->compare('end', $this->end, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('question_id', $this->question_id);
        $criteria->compare('disk_id', $this->disk_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Tries to find the active user attempt with the disk and creates one if there's none
     *
     * @param Disk $disk Disk to work with
     *
     * @return UserAttempt An existing or new attempt
     */
    public static function getCreateAttemptByDisk(Disk $disk)
    {
        $activeAttempt = UserAttempt::model()->with('userAnswers', 'userAnswers.question')->findByAttributes(
            array(
                "user_id" => Yii::app()->user->id,
                "disk_id" => $disk->id,
                "end" => '0000-00-00 00:00:00',
            )
        );
        if ($activeAttempt === null) {
            $activeAttempt = new UserAttempt();
            $activeAttempt->user_id = Yii::app()->user->id;
            $activeAttempt->disk_id = $disk->id;
            $activeAttempt->start = date(Yii::app()->params['timeFormat']);
            $activeAttempt->save(false);
        }
        return $activeAttempt;
    }

    /**
     * Returns the currently active attempt of the user. Null if there's no current attempt
     * @static
     *
     * @param Disk $disk
     *
     * @return UserAttempt
     */
    public static function getActiveAttemptByDisk(Disk $disk)
    {
        $activeAttempt = UserAttempt::model()->findByAttributes(
            array(
                "user_id" => Yii::app()->user->id,
                "disk_id" => $disk->id,
                "end" => '0000-00-00 00:00:00',
            )
        );
        return $activeAttempt;
    }

    public static function createAttemptByDisk(Disk $disk)
    {
        $newAttempt = new UserAttempt();
        $newAttempt->user_id = Yii::app()->user->id;
        $newAttempt->disk_id = $disk->id;
        $newAttempt->start = date(Yii::app()->params['timeFormat']);
        $newAttempt->save(false);
        return $newAttempt;
    }

    public function finishAttempt()
    {
        $this->end = date(Yii::app()->params['timeFormat']);
        $this->save(false);
    }

    public function getCorrectAnswersCount()
    {
        $count = 0;
        foreach ($this->userAnswers as $answer) {
            $count += $answer->answer->correct;
        }
        return $count;
    }

    /**
     * @param string $condition
     * @param array  $params
     *
     * @return UserAttempt
     */
    public function find($condition = '', $params = array())
    {
        return parent::find($condition, $params);
    }


    public static function getLastAttemptByDisk($disk)
    {
        $condition = self::getDiskAttemptsCondition($disk);
        $attempt = UserAttempt::model()->find($condition);
        return $attempt;
    }

    private static function getDiskAttemptsCondition($disk)
    {
        $condition = new CDbCriteria();
        $condition->condition = "disk_id=:disk_id AND user_id=:user_id AND end<>:end";
        $condition->params = array(
            'disk_id' => $disk->id,
            'user_id' => Yii::app()->user->id,
            'end' => '0000-00-00 00:00:00',
        );
        $condition->order = "start desc";
        return $condition;
    }

    public function getAllAttemptsByDisk($disk)
    {
        $attempts = UserAttempt::model()->findAll($this->getDiskAttemptsCondition($disk));
        return $attempts;
    }

    /**
     * @param $question Question
     *
     * @return Answer|null The answer of the user from the current attempt. Null if there's been no answer given yet
     */
    public function getAnswerForQuestion(Question $question)
    {
        foreach ($this->userAnswers as $userAnswer) {
            if ($userAnswer->question->id == $question->id) {
                return $userAnswer->answer;
            }
        }
        return null;
    }

    /**
     * Static function similar to #getCorrectAnswersCount.
     * @static
     *
     * @param $disk Disk to look for correct answers in
     *
     * @return int Number of correct answers in the disk
     */
    public static function getCorrectAnswersCountByDisk($disk)
    {
        $attempt = UserAttempt::getLastAttemptByDisk($disk);
        $correct = $attempt != null ? $attempt->getCorrectAnswersCount() : 0;
        return $correct;
    }

    /**
     * Static function similar to #getCorrectAnswersCountByDisk but working with a list of disks ids.
     * @static
     *
     * @param $disk array of disks ids to look for correct answers in
     *
     * @return array Array of disk id => number of correct answers in the disk
     */
    public static function getCorrectAnswersCountByDiskArray($disks)
    {
        $tableName = UserAttempt::model()->tableName();
        $query = Yii::app()->db->createCommand();
        $query->select('disk_id disk, max(id) attempt')
            ->from($tableName)
            ->where(array('in', 'disk_id', $disks))
            ->andWhere('user_id=:user_id AND end<>:end', array(
                    'user_id' => Yii::app()->user->id,
                    'end' => '0000-00-00 00:00:00',
                ))
            ->group('disk_id');
        $result = $query->queryAll(true);

        $results = array();
        foreach($result as $entry) {
            $results[$entry['disk']] = $entry['attempt'];
        }

        $attempts = array_values($results);
        $criteria = new CDbCriteria();
        $criteria->addInCondition("attempt_id", $attempts);
        $userAnswers = UserAnswer::model()->with('answer')->findAll($criteria);

        $attempts = array_fill_keys($attempts, 0);
        foreach($userAnswers as $userAnswer) {
            $attempts[$userAnswer->attempt_id] += $userAnswer->answer->correct;
        }

        foreach($results as $disk=>$attempt) {
            $results[$disk] = $attempts[$attempt];
        }

        //die(print_r($results, false));

        return $results;

        /*$attempt = UserAttempt::getLastAttemptByDisk($disk);
        $correct = $attempt != null ? $attempt->getCorrectAnswersCount() : 0;
        return $correct;*/
    }

    public function isActive()
    {
        return $this->end == '0000-00-00 00:00:00';
    }

    /**
     * Returns the number of the first question the user hasn't unswered to
     * @return int
     * @todo Complete the function
     */
    public function getFirstUnansweredQuestion()
    {
        if (!$this->isActive()) {
            return 1;
        }

        return 1;
    }
}