<?php

/**
 * This is the model class for table "{{test_category}}".
 *
 * The followings are the available columns in table '{{test_category}}':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $full_description
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $base_code
 *
 * The followings are the available model relations:
 * @property Disk[] $disks
 * @property PayedGroup[] $payedGroups
 */
class Category extends CActiveRecordTimed
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return boolean If the category available for the current user (he's bought it)
	 */
	public function isAvailableForUser() {
		$allFree = Yii::app()->params['allCategoriesOpened'] === true;
		return count($this->payedGroups) != 0 || $allFree;
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{test_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name, base_code, seo_title, seo_keywords', 'length', 'max'=>255),
			array('description, base_code, seo_title, seo_description, full_description, seo_keywords', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, full_description, seo_title, seo_description, seo_keywords', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @param mixed  $pk
	 * @param string $condition
	 * @param array  $params
	 *
	 * @return Category
	 */
	public function findByPk($pk, $condition = '', $params = array())
	{
		return parent::findByPk($pk, $condition, $params);
	}


	/**
	 * @return Category
	 */
	public function with()
	{
		return parent::with();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'disks' => array(self::HAS_MANY, 'Disk', 'test_group_id'),
			'payedGroups' => array(self::HAS_MANY, 'PayedGroup', 'test_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'description' => 'Описание',
			'base_code' => 'Код 1С',
			'full_description' => 'Подробное описание (под списком тестов)',
			'seo_title' => 'Meta Title SEO',
			'seo_description' => 'Meta Description SEO',
			'seo_keywords' => 'Meta Keywords',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('seo_title',$this->description,true);
		$criteria->compare('seo_description',$this->description,true);
		$criteria->compare('full_description',$this->description,true);
		$criteria->compare('seo_keywords',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
