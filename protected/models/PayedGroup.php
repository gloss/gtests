<?php

/**
 * This is the model class for table "{{payed_group}}".
 *
 * The followings are the available columns in table '{{payed_group}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $test_category_id
 * @property string $payed_date
 * @property double $sum
 *
 * The followings are the available model relations:
 * @property Category $testCategory
 * @property User $user
 */
class PayedGroup extends UseredModel
{
	/**
	 * Writes that a new category is payed for by the current user
	 * @static
	 *
	 * @param $category Category
	 * @param $sum double
	 *
	 * @throw CHttpException If the user is not authorized
	 */
	public static function addPayedGroup(Category $category, $sum) {
		if(!Yii::app()->user)
			throw new CHttpException("403", "Пользователь не авторизован");
		$group = new PayedGroup();
		$group->payed_date = date(Yii::app()->params['timeFormat']);
		$group->sum = $sum;
		$group->test_category_id = $category->id;
		$group->user_id = Yii::app()->user->id;
		$group->save(false);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PayedGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{payed_group}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, test_category_id, payed_date', 'required'),
			array('user_id, test_category_id', 'numerical', 'integerOnly'=>true),
			array('sum', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, test_category_id, payed_date, sum', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'testCategory' => array(self::BELONGS_TO, 'Category', 'test_category_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'test_category_id' => 'Test Category',
			'payed_date' => 'Payed Date',
			'sum' => 'Sum',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('test_category_id',$this->test_category_id);
		$criteria->compare('payed_date',$this->payed_date,true);
		$criteria->compare('sum',$this->sum);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}