<?php
/**
 * Created by JetBrains PhpStorm.
 * User: stan
 * Date: 20.01.13
 * Time: 12:08
 * To change this template use File | Settings | File Templates.
 */
class ExplainsModel extends CFormModel
{
    public $explanation;
    public $id;
    public $disk_id;
    public $text;
    public $attempts;
    public $incorrect;
    public $proportion;
    public $groupQuestion;

    public function rules() {
        return array(
            array('id, text, disk_id, explanation, groupQuestion, proportion', 'safe')
        );
    }

    public function getSqlDataProvider() {
        $pageSize = 20;
        //$total = Question::model()->count();
        $query = Yii::app()->db->createCommand();

        // Subquery is for getting info from the Questions table with the given conditions
        $subQuery = Yii::app()->db->createCommand();
        $subQuery->select('*')->from('{{question}} q');
        // Any constrains on the Questions table only should go here!
        $whereQ = '1=1';
        if(!empty($this->explanation) || $this->explanation == "0") {
            $whereQ .= ' AND q.explanation IS ' . ($this->explanation == 0 ? '' : ' NOT ') . ' NULL ';
        }
        if(!empty($this->id)) {
            $whereQ .= ' AND q.id=' . $this->id;
        }
        if(!empty($this->text)) {
            $whereQ .= ' AND q.text LIKE "%' . $this->text . '%"';
        }
        if(!empty($this->disk_id)) {
            $whereQ .= ' AND q.disk_id=' . $this->disk_id;
        }
        $subQuery->where($whereQ);
        // If there are some succeeding conditions in the outer query, query some more data than needed
        // TODO Doesn't work by now... Need to rewrite the CSqlDataProvider a bit to make it work
        //$subQuery->limit($pageSize * 3);

        // groupQuestion is needed to classify the questions in groups by number of the answers given (10s, 100s, etc)
        $query->select = 'q.id, q.disk_id, q.text, NOT q.explanation IS NULL explanation, stat.answers_total attempts, stat.answers_total - stat.answers_correct incorrect,
        ifnull(100 * (1 - stat.correct_proportion), 0) proportion, stat.group_question groupQuestion';
        $query->from = '(' . $subQuery->text . ') q';
        $query->join = 'LEFT JOIN {{answers_stats}} stat ON stat.question_id = q.id';
        //$query->group = 'q.id, q.disk_id, q.text';
        $where = "1=1 ";


        //$having = '1=1 ';
        if(!empty($this->proportion)) {
            $firstLetter = $this->proportion[0];
            if($firstLetter != '<' && $firstLetter != '>' && $firstLetter != '=') {
                $this->proportion = '>' . $this->proportion;
            }
            $where .= ' AND ifnull(100 * (1 - stat.correct_proportion), 0) ' . $this->proportion;
        }
        if(!empty($this->groupQuestion)) {
            $where .= ' AND stat.group_question=' . $this->groupQuestion;
        }
        $query->where = $where;
        $model = Question::model();
        $model->setTableAlias('q');
        $total = $model->count(str_replace('q.', '', $whereQ));
        $data = new CSqlDataProvider($query->getText(), array(
            'totalItemCount'=>$total,
            'sort'=>array(
                'attributes'=>array(
                    'incorrect', 'attempts', 'exp', 'proportion', 'groupQuestion'
                ),
                'defaultOrder'=>array(
                    'groupQuestion'=>CSort::SORT_DESC,
                    'proportion'=>CSort::SORT_DESC,
                    'attempts'=>CSort::SORT_DESC
                ),
            ),
            'pagination'=>array(
                'pageSize'=>$pageSize,
            ),
        ));
        return $data;
    }

}
