<?php

/**
 * This is the model class for table "{{disk}}".
 *
 * The followings are the available columns in table '{{disk}}':
 *
 * @property integer       $id
 * @property string        $name
 * @property integer       $test_group_id
 * @property boolean       $is_free
 * @property string        $base_code
 *
 * The followings are the available model relations:
 * @property Category      $category
 * @property Question[]    $questions
 * @property UserAttempt[] $userAttempts
 */
class Disk extends CActiveRecordTimed
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return Disk the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{disk}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, test_group_id', 'required'),
			array('test_group_id, is_free', 'numerical', 'integerOnly'=> true),
			array('name, base_code', 'length', 'max'=> 255),
			array('base_code, category', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, test_group_id, is_free', 'safe', 'on'=> 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category'     => array(self::BELONGS_TO, 'Category', 'test_group_id'),
			'questions'    => array(self::HAS_MANY, 'Question', 'disk_id'),
			'userAttempts' => array(self::HAS_MANY, 'UserAttempt', 'disk_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'            => 'ID',
			'name'          => 'Name',
			'test_group_id' => 'Test Group',
			'is_free'       => 'Is Free',
			'base_code'     => 'Code of 1C base',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('test_group_id', $this->test_group_id);
		$criteria->compare('is_free', $this->is_free);

		return new CActiveDataProvider($this, array(
												   'criteria'=> $criteria,
											  ));
	}

	/**
	 * @param mixed  $pk
	 * @param string $condition
	 * @param array  $params
	 *
	 * @return Disk
	 */
	public function findByPk($pk, $condition = '', $params = array())
	{
		return parent::findByPk($pk, $condition, $params);
	}


	/**
	 * @return Question The first question in the disk
	 */
	public function getFirstQuestion()
	{
		if (count($this->questions) == 0) {
			return null;
		}
		return $this->questions[0];
	}

	/**
	 * @param Question $question Question which number on the disk should be gotten
	 *
	 * @return null|int The number of the requested question. Null is returned if the question is not from this disk
	 */
	public function getQuestionNumber(Question $question)
	{
        // Search the array for the question by id
        $number = false;
        foreach($this->questions as $key=>$q) {
            if($q->id === $question->id) {
                $number = $key;
                break;
            }
        }
        if ($number === false) {
			return null;
		}
		return $number + 1;
	}

	/**
	 * @param $number Number of the question in the disk
	 *
	 * @return null|Question The question found. Null if number is out of bounds
	 */
	public function getQuestionByNumber($number)
	{
		if ($number > count($this->questions) || $number <= 0) {
			return null;
		}
		return $this->questions[$number - 1];
	}

	public function getNextQuestion($question)
	{
		$number = $this->getQuestionNumber($question);
		if ($number === count($this->questions)) {
			return array(null, 0);
		}
		return array($this->questions[$number], ++$number);
	}

	public function isLastQuestion($question)
	{
		$number = $this->getQuestionNumber($question);
		return $number == count($this->questions);
	}

	public function getQuestionCount()
	{
		return count($this->questions);
	}

	public function setAttribute($name, $value)
	{
		$result = parent::setAttribute($name, $value);
		if ($name === "category" && get_class($value) === "Category") {
			$this->test_group_id = $value->id;
		}
		return $result;
	}

    /**
     * @return int Id of the next disk in the category
     */
    public function getNextDiskId() {
        // TODO Optimize - it wasn't cached
        $disks = $this->category->disks;
        foreach($disks as $key=>$value) {
            if($value->id === $this->id) {
                $key++;
                break;
            }
        }
        return $key < count($disks) ? $disks[$key]->id : 0;
    }
}
