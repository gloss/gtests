<?php

/**
 * This is the model class for table "{{user_answer}}".
 *
 * The followings are the available columns in table '{{user_answer}}':
 * @property integer $id
 * @property string $date
 * @property integer $attempt_id
 * @property integer $answer_id
 * @property integer $question_id
 * @property integer $correct
 *
 * The followings are the available model relations:
 * @property Question $question
 * @property Answer $answer
 * @property UserAttempt $attempt
 * @property Question $id0
 */
class UserAnswer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserAnswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user_answer}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('attempt_id, answer_id, question_id', 'required'),
			array('attempt_id, answer_id, question_id, correct', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, date, attempt_id, answer_id, question_id, correct', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'question' => array(self::BELONGS_TO, 'Question', 'question_id'),
			'answer' => array(self::BELONGS_TO, 'Answer', 'answer_id'),
			'attempt' => array(self::BELONGS_TO, 'UserAttempt', 'attempt_id'),
			'id0' => array(self::BELONGS_TO, 'Question', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'attempt_id' => 'Attempt',
			'answer_id' => 'Answer',
			'question_id' => 'Question',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('attempt_id',$this->attempt_id);
		$criteria->compare('answer_id',$this->answer_id);
		$criteria->compare('question_id',$this->question_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}