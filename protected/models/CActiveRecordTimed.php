<?php
/**
 * Extension to CActiveRecord with autosaving update time
 *
 * @property integer $update_time
 */
class CActiveRecordTimed extends CActiveRecord
{

    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            $this->update_time=new CDbExpression('NOW()');
            return true;
        }
        else
            return false;
    }

}
