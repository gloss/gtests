<?php

/**
 * This is the model class for table "{{question}}".
 *
 * The followings are the available columns in table '{{question}}':
 * @property integer $id
 * @property integer $disk_id
 * @property string $text
 * @property string $internal_code
 * @property string $image
 * @property string $base_code
 * @property integer $order
 * @property string $explanation
 * @property integer $explanation_added
 *
 * The followings are the available model relations:
 * @property Answer[] $answers
 * @property Disk $disk
 * @property UserAttempt[] $userAttempts
 */
class Question extends CActiveRecordTimed
{
    public $attempts;
    public $incorrect;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Question the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{question}}';
	}

    public function getText()
    {
        return htmlspecialchars($this->text);
    }

//  public function __get($name) {
//    if($name=="text") {
//      return htmlspecialchars($this->getAttribute($name));
//    }
//    return parent::__get($name);
//  }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('disk_id, text', 'required'),
			array('disk_id, order', 'numerical', 'integerOnly'=>true),
			array('text, internal_code, image, base_code', 'length', 'max'=>255),
			array('base_code, order, text', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, disk_id, text, internal_code, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answers' => array(self::HAS_MANY, 'Answer', 'question_id'),
			'disk' => array(self::BELONGS_TO, 'Disk', 'disk_id'),
			'userAttempts' => array(self::HAS_MANY, 'UserAttempt', 'question_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'disk_id' => 'Disk',
			'text' => 'Text',
			'internal_code' => 'Internal Code',
			'image' => 'Image',
			'base_code' => 'Code of 1C Base',
			'order' => 'Order of the question within the disk',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('disk_id',$this->disk_id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('internal_code',$this->internal_code,true);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function setAttribute($name, $value)
	{
		$result = parent::setAttribute($name, $value);
		if ($name === "disk" && get_class($value) === "Disk") {
			$this->disk_id = $value->id;
		}
		return $result;
	}

	/**
	 * @return Answer|null The correct answer for this question or null if nothing found
	 */
	public function getCorrectAnswer() {
		foreach($this->answers as $answer) {
			if($answer->correct) {
				return $answer;
			}
		}
		return null;
	}

    /**
     * @param mixed $pk
     * @param string $condition
     * @param array $params
     * @return Question
     */
    public function findByPk($pk, $condition = '', $params = array())
    {
        return parent::findByPk($pk, $condition, $params);
    }

    public function setExplanation($explanation) {
        if(empty($this->explanation)) {
            $this->explanation_added = new CDbExpression('NOW()');
        }
        $this->explanation = $explanation;
    }

    public static function getRandomUnexplained()
    {
        $questionModel = Question::model();
        $sql = 'SELECT q1.id FROM ' . $questionModel->tableName() . ' as q1 JOIN(SELECT FLOOR(RAND() * MAX(id)) id FROM ' .
            $questionModel->tableName() . ') as q2 ON q1.id >= q2.id WHERE q1.`explanation` IS NULL order by q1.id LIMIT 1';
        $rowWithId = Yii::app()->db->createCommand($sql)->queryRow(false);
        return $questionModel->findByPk($rowWithId[0]);
    }

}