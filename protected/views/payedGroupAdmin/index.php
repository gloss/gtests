<?php
/* @var $this PayedGroupAdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Payed Groups',
);

$this->menu=array(
	array('label'=>'Create PayedGroup', 'url'=>array('create')),
	array('label'=>'Manage PayedGroup', 'url'=>array('admin')),
);
?>

<h1>Payed Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
