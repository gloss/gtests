<?php
/* @var $this PayedGroupAdminController */
/* @var $model PayedGroup */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('test_category_id')); ?>:</b>
	<?php echo CHtml::encode($data->test_category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payed_date')); ?>:</b>
	<?php echo CHtml::encode($data->payed_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sum')); ?>:</b>
	<?php echo CHtml::encode($data->sum); ?>
	<br />


</div>