<?php
/* @var $this PayedGroupAdminController */
/* @var $model PayedGroup */

$this->breadcrumbs=array(
	'Payed Groups'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PayedGroup', 'url'=>array('index')),
	array('label'=>'Create PayedGroup', 'url'=>array('create')),
	array('label'=>'Update PayedGroup', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PayedGroup', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PayedGroup', 'url'=>array('admin')),
);
?>

<h1>View PayedGroup #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'test_category_id',
		'payed_date',
		'sum',
	),
)); ?>
