<?php
/* @var $this PayedGroupAdminController */
/* @var $model PayedGroup */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'payed-group-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'test_category_id'); ?>
		<?php echo $form->textField($model,'test_category_id'); ?>
		<?php echo $form->error($model,'test_category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payed_date'); ?>
		<?php echo $form->textField($model,'payed_date'); ?>
		<?php echo $form->error($model,'payed_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sum'); ?>
		<?php echo $form->textField($model,'sum'); ?>
		<?php echo $form->error($model,'sum'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->