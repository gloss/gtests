<?php
/* @var $this PayedGroupAdminController */
/* @var $model PayedGroup */

$this->breadcrumbs=array(
	'Payed Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PayedGroup', 'url'=>array('index')),
	array('label'=>'Manage PayedGroup', 'url'=>array('admin')),
);
?>

<h1>Create PayedGroup</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>