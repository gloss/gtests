<?php
/* @var $this PayedGroupAdminController */
/* @var $model PayedGroup */

$this->breadcrumbs=array(
	'Payed Groups'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PayedGroup', 'url'=>array('index')),
	array('label'=>'Create PayedGroup', 'url'=>array('create')),
	array('label'=>'View PayedGroup', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PayedGroup', 'url'=>array('admin')),
);
?>

<h1>Update PayedGroup <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>