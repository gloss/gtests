<?php
/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 * @date   27.09.12
 */
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/site.css');
Yii::app()->clientScript->registerCssFile(CHtml::normalizeUrl(array('site/css')));
?>
<?php $this->beginContent('//layouts/empty'); ?>

<div class="promotion">
    <ul class="promotion">
		<?php foreach ($this->mainMenu['items'] as $key=> $item) : ?>
        <li class="circle-item" id="item<?php echo $key + 1 ?>">
			<?php if (!isset($item['image'])) {
			$class = ' class="no-image"';
		} else $class = ""?>
			<?php if (isset($item['url'])) { ?>
			<a href="<?php echo CHtml::normalizeUrl(array($item['url'])); ?>"<?php echo $class; ?>>
			<?php } else { ?>
            <span<?php echo $class; ?>>
				<?php } ?>

			<?php echo $item['name']; ?>
			<?php if (!isset($item['url'])) { ?>

			</span>
				<?php } else { ?>
			</a>
				<?php } ?>
        </li>
		<?php endforeach; ?>
    </ul>
    <div class="main-content">
		<?php echo $content; ?>
        <div id="loading"></div>
    </div>

</div>

<?php $this->endContent(); ?>
<?php Yii::app()->clientScript->registerScriptFile(
	Yii::app()->request->baseUrl . '/js/site.js', CClientScript::POS_END
); ?>