<?php
/* @var $this DiskAdminController */
/* @var $model Disk */

$this->breadcrumbs=array(
	'Disks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Disk', 'url'=>array('index')),
	array('label'=>'Manage Disk', 'url'=>array('admin')),
);
?>

<h1>Create Disk</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>