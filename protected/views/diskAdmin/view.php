<?php
/* @var $this DiskAdminController */
/* @var $model Disk */

$this->breadcrumbs=array(
	'Disks'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Disk', 'url'=>array('index')),
	array('label'=>'Create Disk', 'url'=>array('create')),
	array('label'=>'Update Disk', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Disk', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Disk', 'url'=>array('admin')),
);
?>

<h1>View Disk #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'test_group_id',
		'is_free',
	),
)); ?>
