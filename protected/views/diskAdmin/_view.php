<?php
/* @var $this DiskAdminController */
/* @var $model Disk */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('test_group_id')); ?>:</b>
	<?php echo CHtml::encode($data->test_group_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_free')); ?>:</b>
	<?php echo CHtml::encode($data->is_free); ?>
	<br />


</div>