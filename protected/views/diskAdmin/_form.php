<?php
/* @var $this DiskAdminController */
/* @var $model Disk */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'disk-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'test_group_id'); ?>
		<?php echo $form->textField($model,'test_group_id'); ?>
		<?php echo $form->error($model,'test_group_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_free'); ?>
		<?php echo $form->checkBox($model,'is_free'); ?>
		<?php echo $form->error($model,'is_free'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->