<?php
/* @var $this DiskAdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Disks',
);

$this->menu=array(
	array('label'=>'Create Disk', 'url'=>array('create')),
	array('label'=>'Manage Disk', 'url'=>array('admin')),
);
?>

<h1>Disks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
