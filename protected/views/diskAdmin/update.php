<?php
/* @var $this DiskAdminController */
/* @var $model Disk */

$this->breadcrumbs=array(
	'Disks'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Disk', 'url'=>array('index')),
	array('label'=>'Create Disk', 'url'=>array('create')),
	array('label'=>'View Disk', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Disk', 'url'=>array('admin')),
);
?>

<h1>Update Disk <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>