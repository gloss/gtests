
<!DOCTYPE html>
<html>
<head>
    <title><?php echo CHtml::encode(Yii::app()->name)?> - технические работы</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset='utf-8'>
    <style>
        html, body{
            height: 99%;
            margin: 0;
            padding: 0;
        }

        .block {
            text-align: center;
            /*margin: 20px;*/
        }

        .block:before {
            content: '';
            display: inline-block;
            zoom:1; /* IE 7 Hack starts here*/
            *display:inline;
            height: 100%;
            vertical-align: middle;
            margin-right: -0.25em; /* Adjusts for spacing */
        }

        .centered {
            display: inline-block;
            zoom:1; /* IE 7 Hack starts here*/
            *display:inline;
            vertical-align: middle;
            max-width: 999px;
            padding: 10px 15px;
        }

        .left, .right {
            margin: 0 auto;
        }

        .left {
            max-width: 540px;
        }

        .right {
            max-width: 415px;
            text-align: left;
        }

        .header {
            font-family: Georgia;
            font-size: 2em;
            color: #674A42;
            line-height: 41px;
        }

        .text {
            font-family: Georgia;
            font-size: 1.2em;
            color: #382723;
            line-height: 21px;
        }

        .left img {
            width: 100%;
            max-width: 540px;
            position: relative;
        }

        @media only screen and (min-device-width : 768px) 
		and (max-device-width : 1024px) 
		and (orientation : landscape),(min-width: 1010px) {
            .left {
                float: left;
            }

            .right {
                float: right;
                margin-top: 22px;
		max-width: 405px;
            }
        }
    </style>
</head>
<body>
<div class="block" style="height: 95%;">

    <div class="centered">
        <div class="right">
            <div class="header">Ой... А вот и кофе</div>
            <div class="text">
                <p>В данный момент мы упорно трудимся на благо всеми любимого сервиса "<?php echo CHtml::encode(Yii::app()->name)?>".</p>

                <p>Скоро он снова станет доступен. Если этого не происходит слишком долго, черкните нам пару строк на
                    <a href="mailto:<?php echo Yii::app()->params['adminEmail']?>"><?php echo Yii::app()->params['adminEmail']?></a>.</p>

                <p>А пока у Вас появилась возможность<br/>выпить чашечку вкусного кофе,<br/> что-нибудь почитать или просто подумать о хорошем.</p>
            </div>
        </div>
        <div class="left">
            <?php echo CHtml::image('/images/design/coffee.jpg', 'Время кофе'); ?>
        </div>
        <div style="clear: both;"></div>
    </div>

</div>
</body>
</html>
