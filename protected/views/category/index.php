<?php
/* @var $this CategoryController */
/* @var $categories array */

$this->breadcrumbs = array(
);
$this->pageTitle = "Тесты 1С:Профессионал - онлайн подготовка";
Yii::app()->clientScript->registerMetaTag('Онлайн подготовка к тестированию 1С:Профессионал по платформе, бухгалетрии, управлению торговленй и другим конфигурациям.', 'description');
?>
<h1>Тесты 1С Профессионал - онлайн подготовка</h1>

<?php $this->widget('ShareButtonsWidget'); ?>

<?php if(count($categories) === 0) : ?>
    <p class="empty-list">
        Что-то ничего не загрузили еще...
    </p>
<?php else : ?>
<ul id="category-list">
	<?php foreach ($categories as $category) : ?>
	<?php
    /* @var $category Category */
    $isAvailableForUser = $category->isAvailableForUser();
    ?>
	<li class="category-line<?php if(!$isAvailableForUser) echo " closed"; ?>">
		<a href="<?php echo CHtml::normalizeUrl(array('category/view', 'id'=> $category->id)); ?>">
		<h2 class="category-link">
		<?php echo $category->name; ?>
		<?php if(!$isAvailableForUser) echo "Закрыт"; ?>
		</h2>
<!--        <div class="well">-->
		    <span class="category-description"><?php echo $category->description; ?></span>
<!--        </div>-->
			</a>
	</li>
	<?php endforeach ?>
</ul>


<div id="categories-seo">
<h2>1С тесты онлайн</h2>
<p>В каждой категории собрано большое количество тестов по 1С для подготовки к тестированию 1С:Профессионал. Тесты 1С представляют собой группы по 14 закрытых вопросов, то есть таких, на которые нужно выбрать один ответ из нескольких предложенных вариантов. Тест 1С считается пройденным, если дано не менее 12 правильных ответов.</p>
<p>1С разделяет тесты на несколько групп по областям знаний. На данный момент на сервисе онлайн подготовки к тестам 1С представлены наиболее популярные группы, получение сертфиката по которым дает много преимуществ для Вас. Успешная сдача тестов 1С по платформе, например, позволит Вам подтвердить свои знания основ платформы и откроет двери для сдачи экзамена 1С:Специалист. Тесты 1С по Бухгалтерии предприятия очень полезны для бухгалтера.</p>
</div>
<?php endif; ?>

<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . "/css/category.css");
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . "/css/category.css");
?>
