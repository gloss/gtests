<?php
/* @var $this CategoryController */
/* @var $category Category */
/* @var $disks array */
/* @var $disksDataProvider CActiveDataProvider */
/* @var $fullAccess boolean */
/* @var $pages CPagination */

$this->breadcrumbs = array(
    'Тесты 1С' => array('category/index'),
    $category->name,
);

$meta_title = $category->seo_title;
if (empty($meta_title)) $meta_title = $category->name;
$full_desc = $category->full_description;
if (empty($full_desc)) $full_desc = $category->description;
$meta_desc = $category->seo_description;
if (empty($meta_desc)) $meta_desc = $category->name . ' для подготовки к тестированию 1С:Профессионал';
$meta_keys = $category->seo_keywords;

Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/category-view.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/category-view.css');
$this->pageTitle = $meta_title . " - Онлайн подготовка к тестированию 1С:Профессионал";
Yii::app()->clientScript->registerMetaTag($meta_desc, 'description');
if (!empty($meta_keys))
    Yii::app()->clientScript->registerMetaTag($meta_keys, 'keywords');
if (!empty($pages)) {
    Yii::app()->clientScript->registerLinkTag(
        'canonical',
        null,
        CHtml::normalizeUrl(Array('category/view', 'id' => $category->id, 'all' => 1))
    );
}
?>
<h1>Список тестов раздела "<?php echo $category->name; ?>"</h1>

<?php $this->widget('ShareButtonsWidget'); ?>

<?php if(Yii::app()->getModule("user")->isAdmin()) : ?>
    <div class="admin-action"><?php echo CHtml::link('Редактировать раздел', array('categoryAdmin/update', 'id'=>$category->id)); ?></div>
<?php endif; ?>

<ul id="disks-list">
    <?php
    $disksIds = array();
    foreach($disks as $disk) {
        $disksIds[] = $disk->id;
    }
    $correctCounts = UserAttempt::getCorrectAnswersCountByDiskArray($disksIds);
    foreach ($disks as $disk) : ?>
    <?php /* @var $disk Disk */

    //$correct = UserAttempt::getCorrectAnswersCountByDisk($disk);
    $correct = array_key_exists($disk->id, $correctCounts) ? $correctCounts[$disk->id] : 0;
    $total = $disk->getQuestionCount();
    $completed = $correct === $total; ?>
    <li<?php if ($completed) echo ' class="completed"'; ?>><?php echo CHtml::link($disk->name, array("test/index", "cid" => $category->id, "did" => $disk->id)) ?>
        <?php
        if (!Yii::app()->user->isGuest) : ?>
        <p><?php echo $correct;  ?>/<?php echo $total; ?></p>
        <?php endif; ?></li>
    <?php endforeach ?>
</ul>

<?php if (!empty($pages)) $this->widget('CLinkPager', array('pages' => $pages)); ?>
<div class="page-switcher">
    <?php if (!empty($pages)) {
    echo CHtml::link("Все тесты раздела", array('category/view', 'id' => $category->id, 'all' => 1));
} else {
    echo CHtml::link("По страницам", array('category/view', 'id' => $category->id));
}?>
</div>
<?php if (!$fullAccess) : ?>
<div class="more-disks"><?php echo CHtml::link(
    "Пройти остальные тесты", array("payment/buy", "id" => $category->id)
) ?></div>
<?php endif ?>
<div class="full-description">
    <?php echo $full_desc; ?>
</div>

