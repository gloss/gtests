<?php
/* @var $this TestController */
/* @var $disk Disk */
/* @var $qNumber int */
?>

<div id="disqus_thread"></div>
<script type="text/javascript">
var disqus_shortname = '1c-test'; // required: replace example with your forum shortname
var disqus_url = '<?php echo $this->createAbsoluteUrl("test/question", array("qid"=>$qNumber, "did"=>$disk->id, "cid"=>$disk->category->id));?>';
//console.log(disqus_url);

(function() {
    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
    dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>