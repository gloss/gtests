<?php
/* @var $this TestController */
/* @var $disk Disk */
/* @var $attempts array */

$this->breadcrumbs = array(
    'Тесты 1С' => array('category/index'),
	$disk->category->name    => array('category/view', 'id' => $disk->category->id),
	$disk->name => array('test/index', 'cid'=>$disk->category->id,  'did' => $disk->id),
	'История',
);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/test-result.css');
$totalQuestions = $disk->getQuestionCount();
?>
<h1>Результаты тестирования по <?php echo $disk->category->name . " -> " . $disk->name ?></h1>

<div id="results">
	<table>
		<thead>
		<tr>
			<td>Начало</td>
			<td>Конец</td>
			<td>Результат</td>
			<td></td>
		</tr>
		</thead>
		<tbody>
		<?php foreach($attempts as $attempt) : ?>
			<?php /* @var $attempt UserAttempt */ ?>
		<tr>
			<td><?php echo $attempt->start; ?></td>
			<td><?php echo $attempt->end; ?></td>
			<td><?php echo $attempt->getCorrectAnswersCount() . "/" . $totalQuestions; ?></td>
			<td><?php echo CHtml::link("Детально", array('test/details', 'id' => $attempt->id, 'did' => $disk->id, 'cid' => $disk->category->id)); ?></td>
		</tr>
	<?php endforeach; ?>
		</tbody>
	</table>
</div>
