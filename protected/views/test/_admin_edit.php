<?php
/* @var $this TestController */
/* @var $question Question */
/* @var $qNumber int */
/* @var $viewMode bool */

if(!empty($viewMode) && $viewMode) {
    $goToInfo = array(
        'title' => 'Перейти к вопросу',
        'route' => 'test/question'
    );
} else {
    $goToInfo = array(
        'title' => 'Просмотр вопроса',
        'route' => 'test/view'
    );
}
?>

<?php if(Yii::app()->getModule("user")->isAdmin()) : ?>
    <div class="admin-action">
        <?php echo CHtml::link('Редактировать вопрос', array('questionAdmin/update', 'id'=>$question->id)); ?>
        <?php echo CHtml::link('Редактировать ответы', array('answerAdmin/admin', 'Answer[question_id]'=>$question->id)); ?>
        <?php
            echo CHtml::link(
                $goToInfo['title'],
                array(
                    $goToInfo['route'],
                    'did'=>$question->disk_id,
                    'qid'=>$qNumber,
                    'cid'=>$question->disk->category->id
                )
            );
        ?>
    </div>
<?php endif; ?>