<?php
/* @var $this TestController */
/* @var $disk Disk */

$this->breadcrumbs = array(
    'Тесты 1С' => array('category/index'),
    $disk->category->name  => array('category/view', 'id' => $disk->category->id),
    $disk->name => array('test/index', 'cid'=>$disk->category->id,  'did' => $disk->id),
    'История',
);
$this->pageTitle = 'Результаты тестирования недоступны - ' . Yii::app()->name;
?>
<h1>Результаты тестирования для Вас недоступны</h1>

<p>Кажется, Вы пытаетесь посмотреть результаты тестирования другого пользователя.
    К сожалению, эта функция сейчас недоступна.</p>

<p>Вы можете <?php echo CHtml::link("посмотреть свои результаты", array('test/result', 'did'=>$disk->id, 'cid'=>$disk->category->id)) ?>
    или <?php echo CHtml::link("начать прохождение теста", array('test/question', 'did'=>$disk->id, 'cid'=>$disk->category->id, "qid" => 1)); ?>.</p>
