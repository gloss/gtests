<?php
/* @var $this TestController */
/* @var $disk Disk */

$this->breadcrumbs = array(
    'Тесты 1С' => array('category/index'),
	$disk->category->name     => array('category/view', 'id' => $disk->category->id),
	$disk->name
);
$question = $disk->getFirstQuestion();
$totalCount = $disk->getQuestionCount();
$correctCount = UserAttempt::getCorrectAnswersCountByDisk($disk);
$activeAttempt = UserAttempt::getActiveAttemptByDisk($disk);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/test-index.css');
?>
<h1>Тестирование "<?php echo $disk->name; ?>"</h1>

<?php if(Yii::app()->getModule("user")->isAdmin()) : ?>
    <div class="admin-action"><?php echo CHtml::link('Редактировать диск', array('diskAdmin/update', 'id'=>$disk->id)); ?></div>
<?php endif; ?>

<?php if ($question != null) : ?>
<?php if(!Yii::app()->user->isGuest) : ?>
<div class="results">
	Текущий результат - <?php echo $correctCount; ?> из <?php echo $totalCount; ?>.
</div>
<?php endif;  ?>
<div class="navigation">
	<?php if($activeAttempt) $text = "Продолжить тестирование"; else $text = "Начать тестирование"; ?>
	<?php echo CHtml::link($text, array("question", 'cid'=>$disk->category->id,  "did" => $disk->id, "qid" => 1), array('class' => 'btn btn-primary')); ?>
	<?php if(!Yii::app()->user->isGuest) : ?>
	<?php echo CHtml::link('Посмотреть историю сдачи теста', array('test/result', 'did' => $disk->id, 'cid' => $disk->category->id)); ?>
	<?php endif; ?>
</div>
<?php else : ?>
<p class="empty_list">Нет вопросов :(</p>
<?php endif; ?>

