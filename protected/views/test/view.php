<?php
/* @var $this TestController */
/* @var $disk Disk */
/* @var $question Question */
/* @var $qNumber int */
/* @var $attemptId int */
/* @var $userAnswer int */

$this->breadcrumbs = array(
    'Тесты 1С' => array('category/index'),
    $disk->category->name    => array('category/view', 'id' => $disk->category->id),
    $disk->name => array('test/index', 'cid'=>$disk->category->id,  'did' => $disk->id),
    'Вопрос ' . $qNumber,
);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/test-question.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/test-question.js', CClientScript::POS_END);
?>

<h1>Вопрос <?php echo $qNumber; ?></h1>

<?php echo $this->renderPartial('_admin_edit', array('question'=>$question, 'qNumber'=>$qNumber, 'viewMode'=>true)); ?>

<div id="text"><?php echo $question->getText() ?></div>

<?php if ($question->image != "") : ?>
    <div id="image"><img src="<?php echo Yii::app()->params['imagesDir'] . DIRECTORY_SEPARATOR . $question->image; ?>"
                         alt="<?php echo $question->getText(); ?>"></div>
<?php endif; ?>

<div id="answers">
    <ol class="view">
    <?php foreach($question->answers as $answer): ?>
        <?php
            $class = '';
            if($answer->correct)
                $class = 'correct';
            elseif($answer->id === $userAnswer)
                $class = 'user-incorrect';
        ?>
        <li class="<?php echo $class; ?>"><span><?php echo $answer->getText(); ?></span></li>
    <?php endforeach; ?>
    </ol>
</div>

<?php echo $this->renderPartial('_explain', array('question'=>$question)); ?>

<div id="back-link">
<?php if($attemptId) {
    echo CHtml::link('<- Вернуться к результатам теста', array('test/details', 'cid'=>$disk->category->id, 'did'=>$disk->id, 'id'=>$attemptId));
} ?>
</div>

<?php echo $this->renderPartial('_comments', array('disk'=>$disk, 'qNumber'=>$qNumber)); ?>