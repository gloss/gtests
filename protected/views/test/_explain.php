<?php
/* @var $this TestController */
/* @var $question Question */
?>

<?php if(Yii::app()->params['enableExplanations'] && Yii::app()->getModule('user')->canViewExplanations()) : ?>
    <?php if(!empty($question->explanation)) : ?>
        <div class="spoiler">
            <!--noindex--><a href="#" rel="nofollow" title="Показать подсказку к вопросу">Подсказка</a><!--/noindex-->
            <div class="spoilerCont">
                <?php if(Yii::app()->getModule('user')->isAdmin()) : ?>
                    <?php echo CHtml::link('Изменить', array('explainAdmin/edit', 'id'=>$question->id), array('target'=>'_blank')); ?><br />
                <?php endif; ?>
                <?php echo $question->explanation; ?>
            </div>
        </div>
    <?php elseif(Yii::app()->getModule('user')->isAdmin()): ?>
        <div class="admin-action">
            <?php echo CHtml::link('Добавить комментарий', array('explainAdmin/edit', 'id'=>$question->id), array('target'=>'_blank'));?>
        </div>
    <?php endif; ?>
<?php endif; ?>