<?php
/* @var $this TestController */
/* @var $form CActiveForm */
/* @var $disk Disk */
/* @var $question Question */
/* @var $qNumber int */
/* @var $attempt UserAttempt */
/* @var $questions array */

$this->breadcrumbs = array(
    'Тесты 1С' => array('category/index'),
	$disk->category->name    => array('category/view', 'id' => $disk->category->id),
	$disk->name => array('test/index', 'cid'=>$disk->category->id,  'did' => $disk->id),
	'Вопрос ' . $qNumber,
);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/test-question.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/test-question.js', CClientScript::POS_END);
$userAnswer = new UserAnswer();
//$userAnswer->attempt_id = $attempt->id;
$answerOptions = array();
foreach ($question->answers as $answer) {
	$answerOptions[$answer->id] = $answer->getText();
}
$currentAnswer = $attempt->getAnswerForQuestion($question);
if($currentAnswer) {
	$userAnswer->answer_id = $currentAnswer->id;
}
$questionCount = $disk->getQuestionCount();
$isLastQuestion = $disk->isLastQuestion($question);
?>

<ul class="pagination center">
	<?php if ($qNumber != 1) : ?>
    <li>
		<?php
		echo CHtml::link(
			"<", array("test/question", 'cid'=>$disk->category->id,  'did' => $disk->id, "qid" => $qNumber - 1), array('class' => 'btn')
		); ?>
    </li>
	<?php endif; ?>
	<?php
	$totalNumbers = Yii::app()->params['pageSized']['questionPages'];
	if(!$totalNumbers)
		$totalNumbers = 10;
	$toLeft = min(floor($totalNumbers / 2), $qNumber - 1);
	$toRight = min($totalNumbers - $toLeft - 1, $questionCount - $qNumber);
	if($toLeft + $toRight + 1 < $totalNumbers && $totalNumbers <= $questionCount) {
		$toLeft = $totalNumbers - $toRight - 1;
	}
	?>
    <!--		--><?php //foreach($questions as $key=>$cur) : ?>
	<?php for($key = $qNumber - $toLeft - 1; $key < $qNumber + $toRight; $key++) : ?>
	<?php
	$cur = $questions[$key];
	$curQuestion = $cur['question'];
	$classes = "";
	$active = $question->id === $curQuestion->id;
	if($active)
		$classes .= " active";
	if($cur['answer'] !== null)
		$classes .= " answered";
	?>
    <li class="page<?php echo $classes; ?>">
		<?php echo CHtml::link($key+1, array('test/question', 'cid'=>$disk->category->id,  'did' => $disk->id, 'qid'=>$key+1)); ?>
    </li>
    <!--		--><?php //endforeach; ?>
	<?php endfor; ?>
	<?php if (!$isLastQuestion) : ?>
    <li>
		<?php
		echo CHtml::link(
			">", array("test/question", 'cid'=>$disk->category->id,  "did" => $disk->id, "qid" => $qNumber + 1), array('class' => 'btn')
		); ?>
    </li>
	<?php endif; ?>
</ul>

<h1>Вопрос <?php echo $qNumber; ?> из <?php echo $questionCount; ?></h1>

<?php echo $this->renderPartial('_admin_edit', array('question'=>$question, 'qNumber'=>$qNumber)); ?>

<div id="text"><?php echo $question->text ?></div>
<?php if ($question->image != "") : ?>
<div id="image"><img src="<?php echo Yii::app()->params['imagesDir'] . DIRECTORY_SEPARATOR . $question->image; ?>"
					 alt="<?php echo htmlspecialchars($question->text); ?>"></div>
<?php endif; ?>
<div id="answers">
	<?php
    $actionFinish = array("test/finish", 'cid'=>$disk->category->id, 'did'=>$disk->id);
	if(!$isLastQuestion){
		$action = array("test/question", 'cid'=>$disk->category->id,  "did" => $disk->id, "qid" => $qNumber + 1);
		$nextText = 'Ответить на вопрос';
	} else {
		$action = $actionFinish;
		$nextText = 'Завершить';
	}
	$form = $this->beginWidget(
		'CActiveForm',
		array(
			 'id'                   => 'answer-form',
			 'enableAjaxValidation' => false,
			 'action' => $action
		)
	);
	?>
	<div id="options">
		<?php
		echo $form->radioButtonList(
			$userAnswer,
			'answer_id',
			$answerOptions
		);
		?>
	</div>

	<?php if(!$isLastQuestion) {
        echo CHtml::submitButton('Ответить на вопрос', array('class' => 'btn btn-primary'));
        echo CHtml::link('Завершить тест', $actionFinish, array('class' => 'finish-link'));
    } ?>
	<?php if($isLastQuestion) echo CHtml::submitButton('Завершить тест', array('class' => 'btn ' . ($isLastQuestion ? 'btn-primary' : ''))); ?>

	<?php
	$this->endWidget();
	?>

    <?php echo $this->renderPartial('_explain', array('question'=>$question)); ?>

	<?php echo $this->renderPartial('_comments', array('disk'=>$disk, 'qNumber'=>$qNumber)); ?>

</div>
