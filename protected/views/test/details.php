<?php
/* @var $this TestController */
/* @var $attempt UserAttempt */
/* @var $disk Disk */
/* @var $finish boolean */


//$disk = $attempt->disk;
$this->breadcrumbs = array(
    'Тесты 1С' => array('category/index'),
	$disk->category->name    => array('category/view', 'id' => $disk->category->id),
	$disk->name => array('test/index', 'cid'=>$disk->category->id,  'did' => $disk->id),
	'Детально',
);
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/test-result.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/test-details.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/test-details.js');
$answers = $attempt->userAnswers;
?>
<?php if(empty($finish) || !$finish) : ?>
<h1>Результаты тестирования по <?php echo $disk->category->name . " -> " . $disk->name; ?></h1>
<?php else : ?>
<h1>Завершено тестирование по <?php echo $disk->category->name . " -> " . $disk->name; ?></h1>

<p id="#correct-answers">
    Правильных ответов: <?php echo $attempt->getCorrectAnswersCount(); ?> из <?php echo $disk->getQuestionCount(); ?>.
</p>

<?php endif; ?>

<div id="results">
	<table>
		<thead>
		<tr>
            <td>Номер</td>
			<td>Вопрос</td>
			<td>Ответ</td>
			<td>Правильный ответ</td>
		</tr>
		</thead>
		<tbody>
<?php
$count = 1;
foreach($disk->questions as $question) : ?>
	<?php
	$curAnswer = "Не отвечено";
    $curAnswerId = null;
	$correct = false;
	foreach($answers as $answer) {
		if($answer->question_id === $question->id) {
			$curAnswer = $answer->answer->getText();
            $curAnswerId = $answer->answer->id;
			$correct = $answer->answer->correct;
			break;
		}
	}
	?>
<tr class="<?php echo $correct ? "correct" : "incorrect"; ?>">
    <td><?php echo $count++;?></td>
    <td><?php echo CHtml::link($question->getText(), array('test/view', 'cid'=>$disk->category->id, 'did'=>$disk->id, 'qid'=>$count-1, 'aid'=>$attempt->id, 'qans'=>$curAnswerId)); ?></td>
    <td><?php echo $curAnswer; ?></td>
    <td><p><?php echo $question->getCorrectAnswer()->getText(); ?></p>
    <?php if(Yii::app()->params['enableExplanations'] && Yii::app()->getModule('user')->canViewExplanations()) : ?>
        <?php if(!empty($question->explanation)) : ?>
                <a href="#" rel="nofollow" class="explain" data-id="explain-q-<?php echo $count;?>">Почему такой ответ?</a>
        <?php endif; ?>
    <?php endif; ?>
    </td>
</tr>
    <?php if(Yii::app()->params['enableExplanations'] && Yii::app()->getModule('user')->canViewExplanations()) : ?>
        <?php if(!empty($question->explanation)) : ?>
        <tr class="explanation" id="explain-q-<?php echo $count; ?>"><td colspan="4"><?php echo $question->explanation; ?></td> </tr>
        <?php endif; ?>
    <?php endif; ?>
	<?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php
if(isset($finish) && $finish) { ?>
    <?php $nextDiskId = $disk->getNextDiskId();
    if($nextDiskId > 0) : ?>
<p id="next-link">
    <?php echo CHtml::link('Перейти к следующему тесту', array('test/index', 'did'=>$disk->getNextDiskId())); ?>
</p>
<?php endif; } ?>
<p id="history">
	<?php echo CHtml::link('< Посмотреть историю сдачи теста', array('test/result', 'cid'=>$disk->category->id, 'did' => $disk->id)); ?>
</p>
