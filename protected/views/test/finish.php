<?php
/* @var $this TestController */
/* @var $disk Disk */
/* @var $attempt UserAttempt */

$this->breadcrumbs=array(
    'Тесты 1С' => array('category/index'),
	$disk->category->name    => array('category/view', 'id' => $disk->category->id),
	$disk->name => array('test/index', 'id' => $disk->id),
	'Результат',
);
?>
<h1>Завершено тестирование по <?php echo $disk->category->name . " -> " . $disk->name ?></h1>

<p>
	Правильных ответов: <?php echo $attempt->getCorrectAnswersCount(); ?> из <?php echo $disk->getQuestionCount(); ?>.
</p>
<p>
	<?php echo CHtml::link('Посмотреть историю сдачи теста', array('test/result', 'id' => $disk->id)); ?>
</p>
