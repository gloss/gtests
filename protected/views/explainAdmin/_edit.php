<?php
/* @var $this ExplainAdminController */
/* @var $question Question */
/* @var $message String */
?>

<h1><?php echo $question->disk->category->name;?></h1>

<?php if(!empty($message)) : ?>
    <p id="message"><?php echo $message; ?></p>
<?php endif; ?>
<div id="form-content">
    <div id="right-column">
        <?php echo $question->getText(); ?><br/>
        <?php echo CHtml::link('К вопросу', array('test/view', 'cid'=>$question->disk->category->id, 'did'=>$question->disk->id, 'qid'=>$question->disk->getQuestionNumber($question)), array('target'=>'_blank')); ?>
        <?php if(!empty($question->image)) : ?>
            <img src="<?php echo Yii::app()->params['imagesDir'] . DIRECTORY_SEPARATOR . $question->image; ?>"
                 alt="<?php echo $question->getText(); ?>">
        <?php endif; ?>
        <ul>
            <?php foreach($question->answers as $answer) : ?>
                <li class="<?php if($answer->correct) echo 'correct'; ?>"><?php echo $answer->getText(); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div id="left-column">
        <form name="explain" method="post" action="<?php //echo CHtml::normalizeUrl(array('/explainAdmin/edit', 'id'=>$question->id));?>">
            <input type="hidden" name="question_id" value="<?php echo $question->id;?>">
            <p><label for="explanation-text">Поле для объяснения</label></p>
            <p>
                <?php $this->widget('ImperaviRedactorWidget', array(
                    // the textarea selector
                    'selector' => '.explanation-text',
                    // some options, see http://imperavi.com/redactor/docs/
                    'options' => array(
                        'lang'=>'ru',
                        'imageUpload' => Yii::app()->params['site_url'].'/file/upload',
                    ),
                )); ?>
                <textarea cols="50" rows="20" class="explanation-text" id="explanation-text" name="explanation-text"><?php echo $question->explanation; ?></textarea></p>
            <p><input type="submit"></p>
        </form>
    </div>
</div>

<?php Yii::app()->clientScript->registerCss('admin-explain-edit',
    '
#right-column {
    max-width: 45%;
    padding-left: 40px;
    display:inline-block;
}
#left-column {
    float: left;
    width: 49%;
    display: inline-block;
}
#right-column li.correct {
    color:green;
    font-weight: bold;
}
#left-column textarea {
    max-width: 99%;
    min-width: 200px;
}
');?>