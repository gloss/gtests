<?php
/* @var $this ExplainAdminController */
/* @var $model ExplainsModel */

$this->breadcrumbs=array(
	'Admin Explain',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$model->getSqlDataProvider(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'text:html:Текст вопроса',
        /*array(
            'class'=>'CLinkColumn',
            'header'=>'Текст вопроса',
            'labelExpression'=>'$data["text"]',
            'urlExpression'=>'Yii::app()->createUrl("explainAdmin/edit", array("id"=>$data["id"]))',
        ),*/
        array(
            'name'=>'attempts',
            'header'=>'Попыток',
            'value'=>'$data["attempts"]',
            'filter'=>false,
        ),

        array(
            'name'=>'incorrect',
            'header'=>'Неправильных',
            'value'=>'$data["incorrect"]',
            'filter'=>false,
        ),
        'proportion::Доля ошибок',
        'groupQuestion::Группа',
        array(
            'name'=>'explanation',
            'header'=>'Есть коммент',
            'value'=>'$data["explanation"]',
            'filter'=>array(0=>"Нет", 1=>"Да"),
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{edit}',
            'header'=>'Объяснение',
            'buttons'=>array(
                'edit'=>array(
                    'label'=>'Изменить',
                    'url'=>'Yii::app()->createUrl("explainAdmin/edit", array("id"=>$data["id"]))',
                )
            )
        ),
    ),
));
?>
