<?php
/* @var $this ExplainAdminController */
/* @var $question Question */
/* @var $message String */

$this->breadcrumbs=array(
	'Admin Explain'=>array('/explainAdmin'),
	'Edit' => array('/explainAdmin/edit', 'id'=>$question->id),
    'Random Unexplained'
);
Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget');
?>

<?php
$this->renderPartial(
    '_edit',
    array(
        'question' => $question,
        'message' => $message,
    )
);
?>
<div class="clear"></div>

    <a href="">Следующий вопрос</a>

<?php Yii::app()->clientScript->registerCss('admin-explain-random',
    '
    @media(max-width:800px) {
        #right-column {
            max-width: 100%;
            padding-left: 0;
        }
        #left-column {
            width: 100%;
        }
}
');?>

<?php Yii::app()->clientScript->registerScript('admin-explain-random',
    '
    $(document).ready(function(){
        window.scrollTo(0, $("#form-content").offset().top - 60)
    });
    '
);?>