<?php
/* @var $this ExplainAdminController */
/* @var $question Question */
/* @var $message String */

$this->breadcrumbs=array(
	'Admin Explain'=>array('/explainAdmin'),
	'Edit',
);
Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget');
?>

<?php
$this->renderPartial(
    '_edit',
    array(
        'question' => $question,
        'message' => $message,
    )
);
?>



