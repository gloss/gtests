<?php
/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 * @date   25.08.12
 */
/* @var $this LoaderAdminController */
?>

<div id="load-xml">
	Load items from XML:
	<?php
	echo CHtml::beginForm(array('loaderAdmin/loadXml'));
	echo(CHtml::label('File name', 'file'));
	echo(CHtml::textField('OPTIONS[file]', 'tests.xml'));
	echo(CHtml::submitButton('Load!'));
	echo(CHtml::endForm());
	?>
</div>