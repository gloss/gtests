<?php
/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 * @date   16.09.12
 */
/* @var $mainMenu array */
header('Content-type: text/css');
$numElements = count($mainMenu['items']);
$dimension = $mainMenu['areaDimension'];
$circle = $mainMenu['circleDimension'];

$increase = pi() * 2 / $numElements;
$angle = pi() * 3 / 2;
$x0 = $dimension / 2;
$y0 = $dimension / 2;
?>

.circle-item {
	margin: 20px 0;
}

.circle-item a {
	padding: 20px;
}

@media(min-width: <?php echo $dimension; ?>px) {
<?php for ($i = 1; $i <= $numElements; $i++) :
	$x = ($dimension / 2.5) * cos($angle) + ($x0);
	$y = ($dimension / 2.5) * sin($angle) + ($y0);
	$left = ($x - $circle / 2);
	$top = ($y - $circle / 2) - ($circle * ($i - 1));
	?>
#item<?php echo $i; ?> {
	left: <?php echo $left; ?>px;
	top: <?php echo $top; ?>px;
<?php if(array_key_exists('image', $mainMenu['items'][$i-1])) : ?>
	background-image: url('<?php echo CHtml::normalizeUrl(array($mainMenu['items'][$i-1]['image'])); ?>');
<?php endif; ?>
}

<?php
	$angle += $increase;
endfor;
$innerDiameter = $dimension - $circle * 2;
$innerAreaWidth = sqrt(pow($innerDiameter, 2) / 2);
$topMargin = 10;
?>

.circle-item {
	width: <?php echo $circle; ?>px;
	height: <?php echo $circle; ?>px;
	position: relative;
	-webkit-border-radius: 999px;
	-moz-border-radius: 999px;
	border-radius: 999px;
	margin: 0;
	padding: 0;
}

.circle-item > a, .circle-item > span {
	padding-top: 8px;
	-webkit-border-radius: 999px;
	-moz-border-radius: 999px;
	border-radius: 999px;
}

.promotion {
	width: <?php echo $dimension; ?>px;
	height: <?php echo $dimension; ?>px;
	margin: <?php echo $topMargin; ?>px auto;
}

.main-content {
	width: <?php echo $innerAreaWidth; ?>px;
	height: <?php echo $innerAreaWidth; ?>px;
	left: <?php echo $x0 - $innerAreaWidth / 2; ?>px;
	top: <?php echo $y0  - $innerAreaWidth / 2 - $dimension - $topMargin; ?>px;
	position: relative;
}
}