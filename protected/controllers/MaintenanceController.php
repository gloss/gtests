<?php

class MaintenanceController extends CController
{
    public function actionIndex()
    {
        header('HTTP/1.1 503 Service Temporarily Unavailable',true,503);
        header('Status: 503 Service Temporarily Unavailable');
        header('Retry-After: 172800');
        $this->renderPartial("index");
    }
}