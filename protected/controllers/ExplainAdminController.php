<?php

class ExplainAdminController extends AdminController
{

	public function actionEdit($id)
	{
        $message = $this->saveExplanation($id);
        $question = Question::model()->with('answers', 'disk', 'disk.category')->findByPk($id);
		$this->render(
            'edit',
            array(
                'question'=>$question,
                'message'=>$message,
            )
        );
	}

	public function actionIndex()
	{
        $model = new ExplainsModel();
        $model->unsetAttributes();
        if(isset($_GET['ExplainsModel'])) {
            $model->attributes = $_GET['ExplainsModel'];
        } else {
            // Don't show explained quesions by default
            $model->explanation = 0;
        }
        $this->render('index', array(
            'model'=>$model,
        ));
	}

    public function actionRandom()
    {
        Yii::app()->theme = 'twitter';

        $question = null;
        $message = "";
        if(isset($_POST['question_id'])) {
            $error = false;
            $q = null;
            $id = $_POST['question_id'];
            $message = $this->saveExplanation($id, $error, $q);
            if($error) $question = $q; else $message .= '. ' . CHtml::link('Посмотреть', array('/explainAdmin/edit', 'id'=>$id), array('target'=>'_blank'));
        }
        if(!$question) $question = Question::getRandomUnexplained();
        $this->render('random', array(
            'question' => $question,
            'message' => $message,
        ));
    }

    protected function saveExplanation($id, $error = false, $question = null)
    {
        $message = "";
        if(isset($_POST['explanation-text']) && $_POST['explanation-text'] !== "") {
            $q = Question::model()->findByPk($id);
            $errors = '';
            if($q !== null) {
                $q->setExplanation($_POST['explanation-text']);
                $result = $q->save();
                if(!$result) $errors = $q->getErrors();
            } else {
                $errors = 'The question with id "' . $id . '" is not found.';
            }
            if(!empty($errors)) {
                $message = "Возникла ошибка: " . $errors;
                $question = $q;
            } else {
                $message = "Информация успешно обновлена";
            }
        }
        return $message;
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}