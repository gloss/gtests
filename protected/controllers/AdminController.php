<?php
/*
 * This file is part of gFortune.
 *
 * gFortune is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gFortune is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with gFortune.  If not, see <http://www.gnu.org/licenses/agpl.html>.
 */
/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 * @date   23.08.12
 */
abstract class AdminController extends BaseController
{
    /**
     * @var bool Defines whether the ads should be hidden on the pages of the controller
     */
    public $hideAds = true;

	public final function filters() {
		$filters = $this->thisFilters();
		$cur = array(
			'accessControl',
		);
		if(is_array($filters)) {
			$filters = array_merge($cur, $filters);
		} else {
			$filters = $cur;
		}
		return $filters;
	}

	public function thisFilters()
	{
		return array();
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('stan'),
				  'expression'=> "Yii::app()->getModule('user')->isAdmin()",
			),
			array('deny', // deny all users
				  'users'=> array('*'),
			),
		);
	}

    public function init()
    {
        Yii::app()->theme = null;
        parent::init();
    }


}