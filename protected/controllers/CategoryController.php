<?php

/**
 *
 */
class CategoryController extends BaseController
{
    protected $MAX_DISKS_UNPAID = 1;

    public function filters()
    {
        $arr = array(
            array(
                'COutputCacheCustom + index',
                'duration' => 1000,
                'dependency' => array(
                    'class' => 'CDbCacheDependency',
                    'sql' => "SELECT MAX(`update_time`) FROM {{test_category}}",
                )
            ),
        );
        return $arr;
    }

    public function actionIndex()
    {
        $categories = Category::model()->with('payedGroups')->findAll();
        $this->render(
            'index',
            array(
                'categories' => $categories
            )
        );
    }

    /**
     * @param $id int
     */
    public function actionView($id, $all = null)
    {
//		$category = Category::model()->with('disks')->findByPk($id);
//		$disks = $category->disks;
//		$isAvailableForUser = $category->isAvailableForUser();
//		if (!$isAvailableForUser) {
//			foreach ($disks as $key=> $disk) {
//				if (!$disk->is_free) {
//					unset($disks[$key]);
//				}
//			}
//
//		}
        $category = Category::model()->cache(CACHE_TIME)->findByPk($id);
        // Turned off to make one less query on the page. It's not used now anyway
        //$isAvailableForUser = $category->isAvailableForUser();
        $isAvailableForUser = true;

        $criteria = new CDbCriteria();
        $criteria->compare("test_group_id", $category->id);
        $criteria->alias = 'tg';
        $criteria->order = 'tg.base_code DESC';
        if (!$isAvailableForUser) {
            $criteria->compare("is_free", true);
        }

        $disksDataProvider = new CActiveDataProvider(
            'Disk',
            array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 5,
                ),
            )
        );

        $pages = null;
        if (empty($all)) {
            $count = Disk::model()->cache(CACHE_TIME)->count($criteria);

            $pages = new CPagination($count);
            $pages->pageSize = Yii::app()->params['pageSizes']['diskList'];
            $pages->applyLimit($criteria);
        }

        $disks = Disk::model()->cache(CACHE_TIME)->with('questions')->findAll($criteria);

//		if ($isAvailableForUser) {
//			$disks = array_slice($disks, 0, $this->MAX_DISKS_UNPAID);
//		}
        $this->render(
            'view',
            array(
                'disks' => $disks,
                'fullAccess' => $isAvailableForUser,
                'category' => $category,
                'pages' => $pages,
                //				 'disksDataProvider' => $disksDataProvider
            )
        );
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}
