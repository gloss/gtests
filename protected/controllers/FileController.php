<?php
/**
 * Class FileController manages uploading of incoming files
 */
class FileController extends BaseController {
    /**
     * Stores the file uploaded to the filesystem and returns the link to the file. Used by Redactor.
     * @TODO Improve: size, store to the database, etc.
     * @see http://evseev.me/all/yii-imperavi/
     */
    public function actionUpload() {
        $directory = realpath(Yii::app()->basePath.'/../' . Yii::app()->params['redactorUploadPath'] . '/').'/';
        $file = md5(date('YmdHis')).'.'.pathinfo(@$_FILES['file']['name'], PATHINFO_EXTENSION);

        if (move_uploaded_file(@$_FILES['file']['tmp_name'], $directory.$file)) {
            $array = array(
                'filelink' => '/' . Yii::app()->params['redactorUploadPath'] . '/'.$file
            );
        }

        echo CJSON::encode($array);
        exit ;
    }
}