<?php

class PaymentController extends AuthUserController
{
	/**
	 * @param $id int
	 *
	 * @throws CHttpException
	 */
	public function actionBuy($id)
	{
		$category = $this->getCategoryById($id);
		$this->render(
			'buy',
			array(
				 'category' => $category,
			)
		);
	}

	public function getCategoryById($id)
	{
		$category = Category::model()->findByPk($id);
		if (!$category)
			throw new CHttpException("404", "Извините, такой группы тестов нет.");
		return $category;
	}

	public function actionFinish($id)
	{
		$category = $this->getCategoryById($id);
		PayedGroup::addPayedGroup($category, 100);
		$this->redirect(array("category/view", "id"=>$id));
		$this->render(
			'finish',
			array(
				 'category' => $category
			)
		);
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}