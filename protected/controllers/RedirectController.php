<?php

class RedirectController extends Controller
{
	public function actionIndex($url='/',$terminate=true, $status_code = 302)
	{
        $this->redirect($url, $terminate, $status_code);
	}
}