<?php

class SiteController extends BaseController
{
//	public $layout = 'main-circle';

    public static function getUserLink() {if (Yii::app()->user->isGuest) {
        return CHtml::link(
            "Зарегистрироваться",
            Yii::app()->getModule('user')->registrationUrl, array('class' => 'btn btn-primary')
        ) . ' или '
            . CHtml::link(
                "Войти", Yii::app()->getModule('user')->loginUrl
            );
    } else {
        return 'Вы вошли как ' . ' ' . Yii::app()->user->name . '. ' . CHtml::link(
            "Выйти", Yii::app()->getModule('user')->logoutUrl
        );
    }}

    public function filters()
    {
        $filename = $this->getViewFile('index');
        $lastModified = filemtime($filename);
        //$indexLastModified = Yii::app()->dateFormatter->formatDateTime(Yii::app()->db->createCommand("SELECT MAX(`update_time`) FROM {{test_category}}")->queryScalar());
        $arr = array(
            array(
                'COutputCacheCustom + index',
                'duration'=>1000,
                //'varyByExpression'=>function($cache){return $this->getViewFile('index');}
                'dependency'=>array(
                    'class'=>'CFileCacheDependency',
                    'fileName'=>$filename,
                )
            ),
        );
        if(false && Yii::app()->user->isGuest) {
            $arr2 = array(
                array(
                    'CHttpCacheFilter + index',
                    'lastModified'=>Yii::app()->user->isGuest ? $lastModified : time(),
                ),
            );
            $arr = array_merge($arr, $arr2);
        }
        return $arr;
    }

	public $mainMenu
		= array(
			'items'           => array(
				array(
					'name' => 'Intro',
					'url' => 'site/intro',
				),
				array(
					'name' => '',
					'image' => 'images/some-image.png'
				),
				array(
					'name' => '',
					'url' => 'site/new',
					'image' => 'images/some-image.png'
				),
				array(
					'name' => 'Item 4',
					'url' => 'site/new',
				),
				array(
					'name' => 'Item 5',
					'url' => 'site/new',
				),
				array(
					'name' => 'Item 6',
					'url' => 'site/new',
				),
				array(
					'name' => 'Item 7',
					'url' => 'site/new',
				),
				array(
					'name' => 'Item 8',
					'url' => 'site/new',
				),
				array(
					'name' => 'Item 9',
				),
				array(
					'name' => 'Item 10',
					'url' => 'site/new',
				),
			),
			'areaDimension'   => 900,
			'circleDimension' => 180,
		);

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=> array(
				'class'    => 'CCaptchaAction',
				'backColor'=> 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'   => array(
				'class'=> 'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout = "main";
		$categories = Category::model()->with('payedGroups')->findAll();
		$this->render(
			'index',
			array('categories'=>$categories)
		);
	}

	public function actionAgreement()
	{
		$this->layout = "main";
		$this->render('agreement');
	}

	private function smartRender($name, $options = null) {
		if(Yii::app()->request->isAjaxRequest) {
			$this->renderPartial($name, $options);
		} else {
			$this->render($name, $options);
		}
	}

	public function actionNew()
	{
		$this->smartRender(
			'new'
		);
	}

	public function actionIntro() {
		$this->smartRender(
			'intro'
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model = new ContactForm;
		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];
			if ($model->validate()) {
				$name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
				$subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
				$headers = "From: $name <{$model->email}>\r\n" .
					"Reply-To: {$model->email}\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
				Yii::app()->user->setFlash(
					'contact', 'Thank you for contacting us. We will respond to you as soon as possible.'
				);
				$this->refresh();
			}
		}
		$this->render('contact', array('model'=> $model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		// display the login form
		$this->render('login', array('model'=> $model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionCss()
	{
		$this->renderPartial(
			'css',
			array(
				 'mainMenu' => $this->mainMenu,
			)
		);
	}
}
