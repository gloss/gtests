<?php
class BaseController extends Controller
{
    public function actionCss()
    {
        throw new CHttpException(404, Yii::t(
            'yii', 'Unable to resolve the request "{route}".', array('{route}' => Yii::app()->request->requestUri)
        ));
    }

    public function setBreadcrumbs($array) {
        $this->breadcrumbs = $array;
    }
}
