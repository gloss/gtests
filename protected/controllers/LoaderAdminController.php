<?php
/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 * @date   24.08.12
 * @todo   Make the loading of the files easier
 */
class LoaderAdminController extends AdminController
{

	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionLoadAnswers()
    {
        $fileName = Yii::app()->basePath . DIRECTORY_SEPARATOR . '../upload/xmls/answers.xml';
        if (!file_exists($fileName)) {
            echo("No file: " . $fileName);
            Yii::app()->end(1);
        }
        $xml = simplexml_load_file($fileName);
        set_time_limit(0);
        $count = 0;
        $countChanged = 0;
        $text = "<table><thead><tr><th>Code</th><th>New answer</th><th>Prev Answer</th></tr></thead>";
        foreach($xml as $answerXml) {
            $answerCode = $answerXml->code;
            $answer = Answer::model()->findByAttributes(array('base_code' => $answerCode));
            $text .= "<tr><td>$answerXml->code</td><td>$answerXml->text</td><td>";
            if(!$answer){
                $text .= "Not found";
            } else {
                $prev = $answer->text;
                $answer->text = $answerXml->text;
                if(!$answer->save(false)) {
                    $text .= "Errors: " . implode('; ', $answer->getErrors());
                } else {
                    $text .= $prev;
                    $countChanged++;
                }
            }
            $count++;
            $text .= "</td></tr>";
        }
        $text .= "</table>";
        $text .= "<p>Total elements: $count. Elements found: $countChanged.</p>";
        $this->render('loadXml', array('text' => $text));
    }

    public function actionLoadQuestions()
    {
        $fileName = Yii::app()->basePath . DIRECTORY_SEPARATOR . '../upload/xmls/questions.xml';
        if (!file_exists($fileName)) {
            echo("No file: " . $fileName);
            Yii::app()->end(1);
        }
        $xml = simplexml_load_file($fileName);
        set_time_limit(0);
        $count = 0;
        $countChanged = 0;
        $text = "<table><thead><tr><th>Code</th><th>New Question</th><th>Prev Question</th></tr></thead>";
        foreach($xml as $questionXml) {
            $questionCode = $questionXml->code;
            $question = Question::model()->findByAttributes(array('base_code' => $questionCode));
            $text .= "<tr><td>$questionXml->code</td><td>$questionXml->text</td><td>";
            if(!$question){
                $text .= "Not found";
            } else {
                $prev = $question->text;
                $question->text = $questionXml->text;
                if(!$question->save(false)) {
                    $text .= "Errors: " . implode('; ', $question->getErrors());
                } else {
                    $text .= $prev;
                    $countChanged++;
                }
            }
            $count++;
            $text .= "</td></tr>";
        }
        $text .= "</table>";
        $text .= "<p>Total elements: $count. Elements found: $countChanged.</p>";
        $this->render('loadXml', array('text' => $text));
    }

	public function actionLoadXml()
	{
		$options = $_POST['OPTIONS'];
		if(!isset($options))
			Yii::app()->end(2);
		$file = $options['file'];
		$inputDir = Yii::app()->params['xmlDir'];
		$fileName = $inputDir . DIRECTORY_SEPARATOR . $file;
		if (!file_exists($fileName)) {
			echo("No file: " . $fileName);
			Yii::app()->end(1);
		}
		$xml = simplexml_load_file($fileName);
		$text = "";
		// Extends the maximum execution time of the script to be sure all the data has been loaded correctly
		set_time_limit(0);
		$counts = array(
			'categories' => 0,
			'disks' => 0,
			'questions' => 0,
		);
		foreach($xml as $categoryXml) {
//			$text .= "$categoryXml->name ($categoryXml->code) <br/>";
			$categoryId = $this->fillZeros($categoryXml->code, 4);
			$category = Category::model()->findByAttributes(array('base_code' => $categoryId));
			if($category === null) {
				$category = new Category();
				$category->name = $categoryXml->name;
				$category->base_code = $categoryId;
				if(!$category->save(false)) {
					$this->appendError("Cannot create category \"$categoryXml->name\".", $text, "db-error");
					continue;
				}
			}
			$counts['categories']++;
			$counts['disks'] = 0;
			$text .= $category->name . ":<br/>";
			foreach($categoryXml->disks->disk as $diskXml) {
//				$text .= "&nbsp;&nbsp;&nbsp;&nbsp;$diskXml->name ($diskXml->code) <br/>";
				$diskId = $categoryId . $this->fillZeros($diskXml->code, 4);
				$disk = Disk::model()->findByAttributes(array('base_code' => $diskId));
				if($disk === null) {
					$disk = new Disk();
					$disk->name = $diskXml->name;
					$disk->category = $category;
					$disk->base_code = $diskId;
					if(!$disk->save(false)) {
						$this->appendError("Cannot create disk \"$diskXml->name\"", $text, "db-error");
						continue;
					}
				}
				$counts['disks']++;
				$counts['questions'] = 0;
				foreach($diskXml->questions->question as $questionXml) {
//					$text .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$questionXml->text ($questionXml->code, 1C - $questionXml->code1c) <br/>";
					$questionId = $diskId . $this->fillZeros($questionXml->code, 4);
					$question = Question::model()->findByAttributes(array('base_code' => $questionId));
					if($question === null) {
						$question = new Question();
						$question->text = $questionXml->text;
						$question->disk = $disk;
						$question->base_code = $questionId;
						$question->internal_code = $questionXml->code1c;
						$question->image = $questionXml->image;
						$question->order = $questionXml->code;
						if(!$question->save(false)) {
							$this->appendError("Cannot create question \"$questionXml->text\"", $text, "db-error");
							continue;
						}
					}
					$counts['questions']++;
					foreach($questionXml->answers->answer as $answerXml) {
//						$text .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$answerXml->name ($answerXml->code, correct - $answerXml->correct) <br/>";
						$answerId = $questionId . $this->fillZeros($answerXml->code, 2);
						$answer = Answer::model()->findByAttributes(array('base_code' => $answerId));
						if($answer === null) {
							$answer = new Answer();
							$answer->question = $question;
							$answer->text = $answerXml->name;
							$answer->base_code = $answerId;
							$answer->correct = $answerXml->correct;
							if(!$answer->save(false)) {
								$this->appendError("Cannot create answer \"$answerXml->name\"", $text, "db-error");
								continue;
							}
						}
					}
				}
					$text .= "&nbsp;&nbsp;&nbsp;&nbsp;" . $disk->name . " (" . $counts['questions'] . " questions);<br/>";
//						$text .= "<br/>";
			}
//			$text .= print_r($category, true) . "<br/><br/><br/>";
			$text .= "Disks - " . $counts['disks'] . "<br/>";
			$text .= "<br/><br/>";
		}
		$text .= "Total categories - " . $counts['categories'] . "<br/>";
		$this->render('loadXml', array('text' => $text));

	}

	public function parseZip($fileName)
	{
		$zip = Yii::app()->zip;

//		$zip->makeZip('./','./toto.zip'); // make an ZIP archive
//		var_export($zip->infosZip('./toto.zip'), false); // get infos of this ZIP archive (without files content)
//		var_export($zip->infosZip('./toto.zip')); // get infos of this ZIP archive (with files content)
		$zip->extractZip('./toto.zip', './1/'); //
	}

	/**
	 * @param $code string Code to prepend with zeros
	 * @param $length int Length of the resulting code
	 *
	 * @return string
	 */
	private function fillZeros($code, $length)
	{
		$result = $code;
		while(strlen($result) < $length) {
			$result = '0' . $result;
		}
		return $result;
	}

	private function appendError($message, $text, $class)
	{
		$text .= "<p class='$class'>$message</p>";
	}
}