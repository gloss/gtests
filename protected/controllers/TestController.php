<?php

class TestController extends AuthUserController
{
	protected $unAuthedActions = array(
		'index',
	);

	/**
	 * @param $did int Id of the disk to be tested with
	 */
	public function actionIndex($did)
	{
		$disk = $this->getFilterDiskAvailable($did);
		$this->render(
			'index',
			array(
				 'disk' => $disk,
			)
		);
	}

    /**
     * @param int $did Id of the disk
     * @param int $qid Number of the question on the disk
     * @param int $cid Category id
     * @throws CHttpException
     */
	public function actionQuestion($did, $qid = null, $cid = null)
	{
        if($qid === null) {
            $this->redirect(array('test/question','did' => $did, 'cid' => $cid, 'qid' => 1));
        }
		$disk = $this->getFilterDiskAvailable($did);
		$question = $disk->getQuestionByNumber($qid);
        if ($question == null) {
            throw new CHttpException("404", "Вопрос не найден.");
        }
        $attempt = UserAttempt::getCreateAttemptByDisk($disk);
        $this->writeAnswer($disk, $attempt);
		$questions = array();
        $answersDb = UserAnswer::model()->with('answer')->findAllByAttributes(array('attempt_id' => $attempt->id));
        $answers = array();
        foreach($answersDb as $curAnswer) {
            $answers[$curAnswer->question_id] = $curAnswer;
        }
		foreach ($disk->questions as $cur) {
			/*$answer = UserAnswer::model()->findByAttributes(
				array('question_id' => $cur->id, 'attempt_id' => $attempt->id)
			);*/
            $answer = array_key_exists($cur->id, $answers) ? $answers[$cur->id] : null;
			$questions[] = array(
				'question' => $cur,
				'answer'   => $answer,
			);
		}
//		if ($question != null) {
		$this->render(
			'question',
			array(
				 'question'      => $question,
				 'disk'          => $disk,
				 'qNumber'       => $qid,
				 'attempt'       => $attempt,
				 'questions'     => $questions,
			)
		);
//		} else {
//			$this->redirect(array("test/finish"));
//		}
	}

    /**
     * Shows a question in a view mode
     * @param $did Disk id
     * @param $qid Question id
     * @param null $aid Attempt id to return to the results view
     */
    public function actionView($did, $qid, $aid = null, $cid = null, $qans = null) {
        $disk = $this->getFilterDiskAvailable($did);
        $question = $disk->getQuestionByNumber($qid);
        if ($question == null) {
            throw new CHttpException("404", "Вопрос не найден.");
        }
        $this->render(
            'view',
            array(
                'question' => $question,
                'disk' => $disk,
                'qNumber' => $qid,
                'attemptId' => $aid,
                'userAnswer' => $qans,
            )
        );
    }

	private function writeAnswer(Disk $disk, UserAttempt $attempt)
	{
		if (isset($_POST["UserAnswer"])) {
			$userAnswerPost = $_POST["UserAnswer"];
//			var_dump($userAnswerPost);
			$userAnswer = new UserAnswer();
//			$userAnswer->attributes = $userAnswerPost;
			$answerId = $userAnswerPost["answer_id"];
			if (!isset($answerId) || $answerId === "") {
				// leave the question unanswered
				return;
			}
			$answer = Answer::model()->cache(CACHE_TIME)->findByPk($answerId);
			if ($answer == null) {
				throw new CHttpException("404", "Извините, вариант ответа не найден.");
			}
			if ($disk->id != $answer->question->disk->id) {
				throw new CHttpException("404", "Извините, ответ на вопрос не из запрошенного теста.");
			}
			$existingAnswer = UserAnswer::model()->findByAttributes(
				array(
					 'attempt_id'  => $attempt->id,
					 'question_id' => $answer->question_id,
				)
			);
			if ($existingAnswer != null) {
				$userAnswer = $existingAnswer;
			} else {
				$userAnswer->question_id = $answer->question_id;
				$userAnswer->attempt_id = $attempt->id;
			}
			$userAnswer->answer_id = $answerId;
            $userAnswer->correct = $answer->correct;
			$userAnswer->save(false);
//			list($question, $qid) = $disk->getNextQuestion($question);
		}
	}

	public function actionFinish($did, $cid = null)
	{
		$disk = $this->getFilterDiskAvailable($did);
		if(null == $cid) {
			$cid = $disk->category->id;
		}
		$attempt = UserAttempt::getActiveAttemptByDisk($disk);
		if ($attempt == null) {
			//throw new CHttpException("Нет активных попыток прохождения теста, чтобы его завершить.");
			$this->redirect(Yii::app()->createUrl('test/result',array('did'=>$did, 'cid'=>$cid)));
		}
		$this->writeAnswer($disk, $attempt);
		$attempt->finishAttempt();
		$this->render(
			'details',
			array(
				 'disk'    => $disk,
				 'attempt' => $attempt,
				 'finish'  => true,
			)
		);
	}

	public function actionResult($did, $cid = null)
	{
		$disk = $this->getFilterDiskAvailable($did);
		$attempts = UserAttempt::model()->getAllAttemptsByDisk($disk);
		$this->render(
			'result',
			array(
				 'disk'     => $disk,
				 'attempts' => $attempts,
			)
		);
	}

	public function actionDetails($id, $did = null, $cid = null) {
		$attempt = UserAttempt::model()->findByPk($id);
        $disk = $this->getFilterDiskAvailable($did);
		if($attempt == null) {
			if(null !== $did) {
				//$this->redirect(Yii::app()->createUrl('test/result', array('did'=>$did, 'cid'=>$cid)));
                $this->render(
                    'attempt_denied',
                    array(
                         'disk' => $disk
                    )
                );
                return;
            } else {
				$this->redirect('/');
            }
		}
		$this->render(
			'details',
			array(
				 'attempt' => $attempt,
                 'disk' => $disk,
			)
		);
	}

	/**
	 * @param $diskid int Id of disk to check
     * @return Disk
	 */
	private function getFilterDiskAvailable($diskid)
	{
       // $dependency = new CExpressionDependency('Yii::app()->user->id');
        $dependency = new CDbCacheDependency("SELECT MAX(`update_time`) FROM {{question}} WHERE `disk_id`=:disk");
        $dependency->params = array(':disk' => $diskid);
		$disk = Disk::model()->cache(CACHE_TIME, $dependency)->with(array("category", /*'category.payedGroups', */'questions'=>array('alias'=>'q'),
            'questions.answers'=>array('alias'=>'z', 'order'=>'q.id, ifnull(z.order, 0) ASC, z.id ASC')))->findByPk($diskid);
		if ($disk == null) {
			throw new CHttpException("404", "Диск не найден.");
		}
        // Won't need it in the future, just some performance tuning
		/*if (!$disk->is_free && !$disk->category->isAvailableForUser()) {
			$this->redirect(array("payment/buy", "id" => $disk->category->id));
		}*/
		return $disk;
	}

	// Uncomment the following methods and override them if needed
	/*
public function filters()
{
	// return the filter configuration for this controller, e.g.:
	return array(
		'inlineFilterName',
		array(
			'class'=>'path.to.FilterClass',
			'propertyName'=>'propertyValue',
		),
	);
}

public function actions()
{
	// return external action classes, e.g.:
	return array(
		'action1'=>'path.to.ActionClass',
		'action2'=>array(
			'class'=>'path.to.AnotherActionClass',
			'propertyName'=>'propertyValue',
		),
	);
}
*/
}
