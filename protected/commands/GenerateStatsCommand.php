<?php
/**
 * Generates stats on cron for user answers
 */

class GenerateStatsCommand extends CConsoleCommand {
    public function run($args) {
        set_time_limit(0);
        Yii::app()->db->createCommand()->truncateTable('{{answers_stats}}');
        $sql = '
        INSERT INTO {{answers_stats}} (question_id, answers_total, answers_correct, correct_proportion, group_question, date_updated)
          SELECT `question_id` question_id, count(`id`) answers_total, sum(`correct`) answers_correct, sum(`correct`) / count(`id`) correct_proportion,
            power(10, LENGTH(concat(count(`id`)))-1) group_question, MIN(NOW()) date_updated FROM {{user_answer}} GROUP BY `question_id`
        ';
        Yii::app()->db->createCommand($sql)->execute();
    }
}