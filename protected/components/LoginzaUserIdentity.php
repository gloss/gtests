<?php
class LoginzaUserIdentity extends UserIdentity
{
    protected $id;

    const ERROR_DB=110;

    public function __construct($username="", $password="") {
        parent::__construct($username, $password);
    }

    public function authenticate($loginzaModel = null) {
        /* @var LoginzaModel $loginzaModel */
        if(empty($loginzaModel->identity) || empty($loginzaModel->provider)) {
            return parent::authenticate();
        }

        $criteria = new CDbCriteria();
        $criteria->condition = 'identity=:identity AND provider=:provider';
        $criteria->params = array(
            ':identity' => $loginzaModel->identity,
            ':provider' => $loginzaModel->provider
        );
        $identity = LoginzaUserIdentityModel::model()->with('User')->find($criteria);
        if(null !== $identity) {
            $this->id = $identity->user_id;
            $this->username = (null !== $identity->full_name) ? $identity->full_name : $identity->user->username;
        } else {
            $username = $loginzaModel->identity;
            echo("username: $username\n\n<br/><br/>");
            echo("full_name: $loginzaModel->full_name<br/><br/>");

            $userCriteria = new CDbCriteria();
            $userCriteria->condition = 'username=:username';
            $userCriteria->params = array(
                ':username' => $username
            );
            $user = User::model()->find($userCriteria);
            if(null === $user) {
                $user = new User();
                $user->loginzaLogin = true;
                $user->username = $username;
                $user->email = $loginzaModel->email;
                $result = $user->save();

                if(!$result) {
                    logDetailed(
                        "Error saving user from Loginza login.",
                        "error",
                        "user.components.loginzaWidget",
                        array(
                            'username' => $username,
                            'errors' => $user->getErrors(),
                            'user' => $user,
                            'loginzaModel' => $loginzaModel
                        )
                    );
                    $this->errorCode = LoginzaUserIdentity::ERROR_DB;
                    $this->errorMessage = $user->getErrors();
                    return false;
                }
            }

            $identity = new LoginzaUserIdentityModel();
            $identity->user_id = $user->id;
            $identity->email = $loginzaModel->email;
            $identity->full_name = $loginzaModel->full_name;
            $identity->identity = $loginzaModel->identity;
            $identity->provider = $loginzaModel->provider;
            $result = $identity->save();

            if(!$result) {
                logDetailed(
                    "Error saving identity from Loginza login.",
                    "error",
                    "user.components.loginzaWidget",
                    array(
                        'errors' => $identity->getErrors(),
                        'identity' => $identity,
                        'user' => $user,
                        'loginzaModel' => $loginzaModel
                    )
                );
                $this->errorCode = LoginzaUserIdentity::ERROR_DB;
                $this->errorMessage = $identity->getErrors();
                return false;
            }

            $this->id = $user->id;
            $this->username = (null !== $identity->full_name) ? $identity->full_name : $user->username;
        }
        //$this->isAuthenticated = true;
        $this->errorCode = LoginzaUserIdentity::ERROR_NONE;
        return true;
    }

    public function getId()
    {
        return $this->id;
    }


}
