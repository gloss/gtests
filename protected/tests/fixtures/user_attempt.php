<?php
/* @var $this CDbFixtureManager */
return array(
    'attempt1' => array(
        'text' => 'Attempt 1',
        'user_id' => 1,
        'disk_id' => 1,
        'question_id' => $this->getRecord('questions', 'question1')->id
    ),
);
