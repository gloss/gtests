<?php
/**
 * File is used to preserve the order of the fixtures in the tests array
 *
 * @see http://stackoverflow.com/questions/6350362/relational-fixtures-in-yii#comment16770097_12340979
 */
$users = User::model()->findAll();
if(count($users)==0) {
    $user = new User();
    $user->username = "Test";
    $user->email = "test";
    $user->save();
    $users[] = $user;
}

$categories = Category::model()->findAll();
if(count($categories) == 0) {
    $category = new Category();
    $category->base_code = "1";
    $category->save();
    $categories[] = $category;
}

$disks = Disk::model()->findAll();
if(count($disks) == 0) {
    $disk = new Disk();
    $disk->test_group_id = $categories[0]->id;
    $disk->base_code = "11";
    $disk->save();
    $disks[] = $disk;
}