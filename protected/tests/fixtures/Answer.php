<?php
/* @var $this CDbFixtureManager */
return array(
    'answer1_1' => array(
        'text' => 'Answer 1-1',
        'question_id' => $this->getRecord('questions', 'question1')->id,
        'correct' => 0
    ),
    'answer1_2' => array(
        'text' => 'Answer 1-2',
        'question_id' => $this->getRecord('questions', 'question1')->id,
        'correct' => 1
    ),
    'answer1_3' => array(
        'text' => 'Answer 1-3',
        'question_id' => $this->getRecord('questions', 'question1')->id,
        'correct' => 0
    ),
    'answer2_1' => array(
        'text' => 'Answer 2-1',
        'question_id' => $this->getRecord('questions', 'question2')->id,
        'correct' => 0
    ),
    'answer2_2' => array(
        'text' => 'Answer 2-2',
        'question_id' => $this->getRecord('questions', 'question2')->id,
        'correct' => 1
    ),
);
