<?php

class UserAnswerTest extends CDbTestCase {
    public $fixtures=array(
        'questions'=>'Question',
        'answers'=>'Answer',
        'userattempts'=>'UserAttempt'
    );

    public function setUp(){
        Yii::app()->user->id = User::model()->findAll()[0]->id;
        parent::setUp();
    }

    public function testFixtures(){
        $question1 = $this->questions('question1');
        $this->assertNotNull($question1->answers);
        $this->assertEquals(3, count($question1->answers));
    }

    public function testAnswersAddedToDb(){
        $userAnswer = new UserAnswer();
        $question = $this->questions('question1');
        $userAnswer->question_id = $question->id;
        $userAnswer->answer_id = $question->answers[0]->id;
        $attempt = $this->userattempts('attempt1');
        $userAnswer->attempt_id = $attempt->id;
        $result = $userAnswer->save();
        if(!$result) {
            $errors = $userAnswer->getErrors();
            $this->fail("Model validation failed: \n #{implode($errors)}");
        }

        $userAnswer=UserAnswer::model()->findByPk($userAnswer->id);
        $this->assertNotNull($userAnswer);
    }
}
