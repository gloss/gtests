<?php
return array(
    // this is used in contact page
    'adminEmail'          => 'info@1c-test.ru',
    'timeFormat'          => 'Y-m-d H:i:s',
    // TODO Make absolute links
    'imagesDir'           => '/' . 'images',
    'xmlDir'              =>
    $baseUrl . 'upload'
        . DIRECTORY_SEPARATOR . 'xmls',
    // If all the categories should be available for any user (without purchasing)
    'allCategoriesOpened' => true,
    'pageSizes'           => array(
        'diskList'       => 10,
        'questionsPages' => 10,
    ),
    'enableExplanations'=>true,
    // relative to the server root
    'redactorUploadPath'=>'upload/redactor',
    'sitewideMessages'=>(file_exists(dirname(__FILE__).DIRECTORY_SEPARATOR.'.sitewide_messages.php') ?
        require(dirname(__FILE__).DIRECTORY_SEPARATOR.'.sitewide_messages.php') : null)
);
