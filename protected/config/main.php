<?php
$baseUrl = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;

/* Format:
$dbsettings = array(
	'connectionString' => 'mysql:host=127.0.0.1;dbname=sidelnikov_glike1',
	'emulatePrepare'   => true,
	'username'         => '029900003_glike',
	'password'         => 'AsWq1596rar',
	'charset'          => 'utf8',
	'tablePrefix'      => 'prof_',
);
*/

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('editable', dirname(__FILE__).'/../extensions/x-editable');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'catchAllRequest'=>file_exists(dirname(__FILE__).'/.maintenance')
        && !(isset($_COOKIE['secret']) &&
            $_COOKIE['secret']=="AwA2!2A") ?
        array('maintenance/index') : null,

	'basePath'       => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'           => 'Тестирование 1С:Профессионал',
	'language'       => 'ru',
	'theme'          => 'twitter',

	'onBeginRequest' => function ($event) {
		if (false && !Yii::app()->user->isGuest) {
			Yii::app()->defaultController = 'category';
		}

        if(array_key_exists('flush_cache', $_GET) && $_GET['flush_cache'] === 'Y') {
            set_time_limit(300);
            Yii::app()->cache->flush();
        }

        if(!Yii::app()->getModule('user')->isAdmin()) {
            /*$routes = Yii::app()->getComponent('log')->getRoutes()->toArray();
            $newArray = array();
            foreach($routes as $route) {
                if(!$route instanceof CWebLogRoute) {
                    $newArray[] = $route;
                }
            }
            Yii::app()->getComponent('log')->setRoutes($newArray);
            */
            // Turning off the debug trace for non-admins
            // see http://yiiframework.ru/forum/viewtopic.php?f=3&t=1415#p58895
            foreach( Yii::app()->log->routes as $route ){
                if( $route instanceof CWebLogRoute ){
                    $route->enabled=false;
                }
            }
        }

        // Remove HTML comments
        // http://www.simplecoding.org/php-kak-ubrat-kommentarii-iz-html-razmetki.html#more-1309
        return ob_start(function($html) {
            return preg_replace('/<!--(.*?)-->/', '', $html);
        });
	},

    'onEndRequest' => function($event){
        // Remove HTML comments
        return ob_get_flush();
    },

	// preloading 'log' component
	'preload'        => array('log'),

	// autoloading model and component classes
	'import'         => array(
		'application.models.*',
		'application.components.*',
        'application.extensions.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',
		'application.controllers.*',
        'editable.*'
	),

	'modules'        => array(
		// uncomment the following to enable the Gii tool

		'gii'      => array(
			'class'     => 'system.gii.GiiModule',
			'password'  => 'AsWq1596rar',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters' => array('127.0.0.1'),
		),

		'user'     => array(
			# encrypting method (php hash function)
			'hash'                => 'md5',

			# send activation email
			'sendActivationMail'  => true,

			# allow access for non-activated users
			'loginNotActiv'       => false,

			# activate user on registration (only sendActivationMail = false)
			'activeAfterRegister' => false,

			# automatically login from registration
			'autoLogin'           => true,

			# registration path
			'registrationUrl'     => array('/user/registration'),

			# recovery password path
			'recoveryUrl'         => array('/user/recovery'),

			# login form path
			'loginUrl'            => array('/user/login'),

			# page after login
			'returnUrl'           => array('/category/index'),

			# page after logout
			'returnLogoutUrl'     => array('/user/login'),

            'captchaBackend'      => 'gd'
		),

		'webshell' => array(
			'class'          => 'application.modules.webshell.WebShellModule',
			// when typing 'exit', user will be redirected to this URL
			'exitUrl'        => '/',
			// custom wterm options
			'wtermOptions'   => array(
				// linux-like command prompt
				'PS1' => '%',
			),
			// additional commands (see below)
			'commands'       => array(
				'test' => array('js:function(){return "Hello, world!";}', 'Just a test.'),
			),
			// uncomment to disable yiic
			// 'useYiic' => false,

			// adding custom yiic commands not from protected/commands dir
			'yiicCommandMap' => array(
				'email' => array(
					'class' => 'ext.mailer.MailerCommand',
					'from'  => 'sam@rmcreative.ru',
				),
			),
			'ipFilters'      => false,
		),

	),

	// application components
	'components'     => array(
		'user'          => array(
			// enable cookie-based authentication
//			'class'         => 'WebUser',
			'allowAutoLogin' => true,
			'loginUrl'       => array('/user/login'),
		),
		// uncomment the following to enable URLs in path-format

		'urlManager'    => array(
			'urlFormat'      => 'path',
			'showScriptName' => false,
			'rules'          => require(dirname(__FILE__). DIRECTORY_SEPARATOR . 'routes.php'),
		),

		'db'            => require(dirname(__FILE__).DIRECTORY_SEPARATOR."dbsettings.php"),

		'errorHandler'  => array(
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		),
		'log'           => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
                /*array(
                    'class' => 'CEmailLogRoute',
                    'levels' => 'error',
                    'emails' => 'info@1c-test.ru'
                ),*/
				// uncomment the following to show log messages on web pages
                array(
                    'class'=>'CWebLogRoute',
                    'enabled'=>YII_DEBUG && (YII_DEBUG_PROFILING_EXT || (array_key_exists('explain', $_GET) && $_GET['explain'] === 'Y')),
                ),
			),
		),

		'zip'           => array(
			'class' => 'application.extensions.zip.EZip',
		),

		'widgetFactory' => array(
			'enableSkin' => true,
			'widgets'    => array(
				'CLinkPager'     => array(),
				'CJuiDatePicker' => array(
					'language' => 'ru',
				),
			),
		),

        'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),

        //X-editable config
        'editable' => array(
            'class'     => 'editable.EditableConfig',
            'form'      => 'plain',        //form style: 'bootstrap', 'jqueryui', 'plain'
            'mode'      => 'inline',            //mode: 'popup' or 'inline'
            'defaults'  => array(              //default settings for all editable elements
                'emptytext' => 'Click to edit'
            )
        ),

        'clientScript' => array(
            //'class' => 'application.components.MinifyClientScript'
            'class'=>'ext.minScript.components.ExtMinScript',
            'coreScriptPosition' => CClientScript::POS_END,
        ),

        'viewRenderer' => array(
            'class' => 'application.vendor.yiiext.twig-renderer.ETwigViewRenderer',
            'twigPathAlias' => 'application.vendor.twig.twig.lib.Twig',

            'fileExtension' => '.twig'
        )
    ),

    'controllerMap'=>array(
        'min'=>array(
            'class'=>'ext.minScript.controllers.ExtMinScriptController',
        ),
    ),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'         => require(dirname(__FILE__). DIRECTORY_SEPARATOR . 'params.php'),
);
