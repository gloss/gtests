<?php
return array(
    'name' => array(
        'text' => 'Some message text',  // Required field
        'active' => true,   // Default is true
        'dismissable' => true,   // Default is true
        'class' => 'alert-error',    // Default is alert-info
        'dismissDays' => 1, // Default is 1
        'hideGuests' => true,  // Default is true - should not show the message for guests
        'excludeUrls' => array()
    ),
);
