<?php
return array(
    'admin/<controller:\w+>' => '<controller>Admin',
    'admin/<controller:\w+>/<id:\d+>' => '<controller>Admin/view',
    'admin/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>Admin/<action>',
    'admin/<controller:\w+>/<action:\w+>' => '<controller>Admin/<action>',
    'webshell' => 'webshell',
    'webshell/<controller:\w+>' => 'webshell/<controller>/index',
    'webshell/<controller:\w+>/<action:\w+>' => 'webshell/<controller>/<action>',
    'category' => 'category/index',
    'category/<id:\d+>' => 'category/view',
    'test/<cid:\d+>/<did:\d+>' => 'test/index',
    'test/<cid:\d+>/<did:\d+>/<action:\w+>' => 'test/<action>',
    'questions/view/<qid:\d+>' => 'questions/view',
    'questions/<cid:\d+>/<qid:\d+>' => 'questions/question',
    'questions/<cid:\d+>' => 'questions/index',
    // If you need redirect, use the following. .htaccess is preferable
    /*'questions' => array(
        'redirect/index',
        'defaultParams' => array(
            'url' => 'category/index',
            'status_code' => '301'
        )
    ),*/
    /* For back compatibility <<<<< */
    // Somehow it just gets called
    'test' => 'category/index',
    'test/question/<did:\d+>/<qid:\d+>' => 'test/question',
    'test/details/<id:\d+>' => 'test/details',
    'test/<action:\w+>/<did:\d+>' => 'test/<action>',
    /* >>>>>> For back compatibility */
    'css/<controller:\w+>-gen.css' => '<controller>/css',
    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
);