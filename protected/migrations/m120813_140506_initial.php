<?php

class m120813_140506_initial extends CDbMigration
{
	public function up()
	{
		$standardOption = "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";

		// table: test_category
		$this->createTable(
			"{{test_category}}",
			array(
				 'id'         => 'pk',
				 'name'       => 'string NOT NULL',
				 'description'=> 'text'
			),
			$standardOption
		);

		// table: disk
		$this->createTable(
			"{{disk}}",
			array(
				 'id'             => 'pk',
				 'name'           => 'string NOT NULL',
				 'test_group_id'  => 'integer NOT NULL',
				 'is_free'        => 'boolean'
			),
			$standardOption
		);
		$this->addForeignKey(
			'fk_test_group_disk',
			'{{disk}}',
			'test_group_id',
			'{{test_category}}',
			'id',
			'CASCADE',
			'RESTRICT'
		);

		// table: question
		$this->createTable(
			"{{question}}",
			array(
				 'id'           => 'pk',
				 'disk_id'      => 'integer NOT NULL',
				 'text'         => 'string NOT NULL',
				 'internal_code'=> 'string',
				 'image'        => 'string'
			),
			$standardOption
		);
		$this->addForeignKey(
			'fk_disk_question',
			'{{question}}',
			'disk_id',
			'{{disk}}',
			'id',
			'RESTRICT',
			'CASCADE'
		);

		// table: answer
		$this->createTable(
			'{{answer}}',
			array(
				 'id'         => 'pk',
				 'question_id'=> 'integer NOT NULL',
				 'text'       => 'string NOT NULL',
				 'correct'    => 'integer NOT NULL DEFAULT 0'
			),
			$standardOption
		);
		$this->addForeignKey(
			'fk_question_answer',
			'{{answer}}',
			'question_id',
			'{{question}}',
			'id',
			'CASCADE',
			'CASCADE'
		);

		// table: user_attempt
		$this->createTable(
			'{{user_attempt}}',
			array(
				 'id'         => 'pk',
				 'start'      => 'timestamp NOT NULL',
				 'end'        => 'timestamp',
				 'user_id'    => 'integer NOT NULL',
				 'disk_id'    => 'integer NOT NULL'
			),
			$standardOption
		);
		$this->addForeignKey(
			'fk_user_user_attempt',
			'{{user_attempt}}',
			'user_id',
			'{{users}}',
			'id',
			'CASCADE',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk_disk_user_attempt',
			'{{user_attempt}}',
			'disk_id',
			'{{disk}}',
			'id',
			'CASCADE',
			'RESTRICT'
		);

		// table: user_answer
		$this->createTable(
			'{{user_answer}}',
			array(
				 'id'        => 'pk',
				 'date'      => 'timestamp',
				 'attempt_id'=> 'integer NOT NULL',
				 'answer_id' => 'integer NOT NULL',
				 'question_id'=> 'integer NOT NULL',
			),
			$standardOption
		);
		$this->addForeignKey(
			'fk_question_user_answer',
			'{{user_answer}}',
			'question_id',
			'{{question}}',
			'id',
			'RESTRICT',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk_user_attempt_user_answer',
			'{{user_answer}}',
			'attempt_id',
			'{{user_attempt}}',
			'id',
			'CASCADE',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk_answer_user_answer',
			'{{user_answer}}',
			'answer_id',
			'{{answer}}',
			'id',
			'RESTRICT',
			'CASCADE'
		);

		// table: payed_group
		$this->createTable(
			'{{payed_group}}',
			array(
				 'id'              => 'pk',
				 'user_id'         => 'integer NOT NULL',
				 'test_category_id'=> 'integer NOT NULL',
				 'payed_date'      => 'timestamp NOT NULL',
				 'sum'             => 'float'
			),
			$standardOption
		);
		$this->addForeignKey(
			'fk_user_payed_group',
			'{{payed_group}}',
			'user_id',
			'{{users}}',
			'id',
			'CASCADE',
			'CASCADE'
		);
		$this->addForeignKey(
			'fk_test_category_payed_group',
			'{{payed_group}}',
			'test_category_id',
			'{{test_category}}',
			'id',
			'RESTRICT',
			'CASCADE'
		);
	}

	public function down()
	{
		$this->dropTable('{{user_answer}}');
		$this->dropTable('{{user_attempt}}');
		$this->dropTable('{{payed_group}}');
		$this->dropTable('{{answer}}');
		$this->dropTable('{{question}}');
		$this->dropTable('{{disk}}');
		$this->dropTable('{{test_category}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}