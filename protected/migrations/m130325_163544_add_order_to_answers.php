<?php

class m130325_163544_add_order_to_answers extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{answer}}', 'order', 'integer');
    }

    public function down()
	{
		$this->dropColumn('{{answer}}', 'order');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}