<?php

class m120825_162034_loadXml extends CDbMigration
{
	private $columnName = 'base_code';
	private $tables
		= array(
			'test_category',
			'disk',
			'question',
			'answer'
		);

	public function up()
	{
		foreach ($this->tables as $table) {
			$this->addColumn("{{" . $table . "}}", $this->columnName, "string");
			$this->createIndex("i_" . $table . "_" . $this->columnName, "{{" . $table . "}}", $this->columnName, true);
		}
		$this->addColumn('{{question}}', 'order', 'tinyint');
	}

	public function down()
	{
//		echo "m120825_162034_loadXml does not support migration down.\n";
//		return false;
		foreach ($this->tables as $table) {
			$this->dropColumn("{{" . $table . "}}", $this->columnName);
		}
		$this->dropColumn('{{question}}', 'order');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}