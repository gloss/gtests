<?php

class m130326_082826_add_correct_to_user_answer extends CDbMigration
{
	public function up()
	{
        set_time_limit(0);
        $this->addColumn('{{user_answer}}', 'correct', 'integer');
        echo "Querying current user answers...\n";

	$row = Yii::app()->db->createCommand('select count(*) from {{user_answer}}')->queryRow(false);
        $count = $row[0];
        $limit = 1000;
        for($i = 0; $i*$limit < $count; $i++) {
            $selectUserAnswers = Yii::app()->db->createCommand()->
                select('ua.id, a.correct')->
                from('{{user_answer}} ua')->
                join('{{answer}} a', 'ua.answer_id=a.id')->
                order('ua.id');
            $selectUserAnswers->limit($limit);
            echo $i*$limit . ".." . ($i+1)*$limit . " out of $count\n";
            $offset = $i * $limit;
            $selectUserAnswers->offset($offset);
            $uas = $selectUserAnswers->queryAll();
            $values = '';
            foreach($uas as $answer) {
                $values .= '(' . $answer['id'] . ',' . $answer['correct'] . '),';
            }
            $values = rtrim($values, ',');
            echo $values . "\n";
            $updateSql = "INSERT INTO {{user_answer}} (id, correct) VALUES $values ON DUPLICATE KEY UPDATE correct=VALUES(correct)";
            echo "Updating correct column...\n";
            Yii::app()->db->createCommand($updateSql)->execute();

        }
	}

	public function down()
	{
		$this->dropColumn('{{user_answer}}', 'correct');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
