<?php

class m130119_174318_question_explain extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{question}}', 'explanation', 'text');
        $this->addColumn('{{question}}', 'explanation_added', 'timestamp');
	}

	public function down()
	{
		$this->dropColumn('{{question}}', 'explanation');
		$this->dropColumn('{{question}}', 'explanation_added');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}