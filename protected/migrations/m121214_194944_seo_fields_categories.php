<?php

class m121214_194944_seo_fields_categories extends CDbMigration
{
	public function up()
	{
		$this->addColumn(
			'{{test_category}}',
			'full_description',
			'text'
		);
		$this->addColumn(
			'{{test_category}}',
			'seo_title',
			'string'
		);
		$this->addColumn(
			'{{test_category}}',
			'seo_description',
			'text'
		);
		$this->addColumn(
			'{{test_category}}',
			'seo_keywords',
			'text'
		);
	}

	public function down()
	{
		$this->dropColumn('{{test_category}}', 'full_description');
		$this->dropColumn('{{test_category}}', 'seo_title');
		$this->dropColumn('{{test_category}}', 'seo_description');
		$this->dropColumn('{{test_category}}', 'seo_keywords');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
