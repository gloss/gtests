<?php

class m130111_034656_add_update_time extends CDbMigration
{
    private $tables = array('{{test_category}}', '{{disk}}', '{{question}}', '{{answer}}');
	public function up()
	{
        $time = time();
        foreach($this->tables as $table) {
            $this->addColumn(
                $table,
                'update_time',
                'timestamp'
            );
            $this->update(
                $table,
                array('update_time'=>new CDbExpression('NOW()'))
            );
        }
	}

	public function down()
	{
		foreach($this->tables as $table) {
            $this->dropColumn($table, 'update_time');
        }
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}