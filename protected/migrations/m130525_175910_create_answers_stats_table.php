<?php

class m130525_175910_create_answers_stats_table extends CDbMigration
{
	public function up()
	{
        $this->createTable(
            '{{answers_stats}}',
            array(
                'id'=>'pk',
                'question_id'=>'integer NOT NULL',
                'answers_total'=>'integer',
                'answers_correct'=>'integer',
                'group_question'=>'integer',
                'correct_proportion'=>'float',
                'date_updated'=>'timestamp NOT NULL'
            )
        );

        $this->addForeignKey(
            'fk_answers_stats_question',
            '{{answers_stats}}',
            'question_id',
            '{{question}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
	}

	public function down()
	{
		$this->dropTable('{{answers_stats}}');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}