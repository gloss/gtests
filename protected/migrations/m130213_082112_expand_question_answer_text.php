<?php

class m130213_082112_expand_question_answer_text extends CDbMigration
{
	public function up()
	{
        $this->alterColumn(
            '{{answer}}',
            'text',
            'text'
        );

        $this->alterColumn(
            '{{question}}',
            'text',
            'text'
        );
	}

	public function down()
	{
//		echo "m130213_082112_expand_question_answer_text does not support migration down.\n";
//		return false;
        $this->alterColumn(
            '{{answer}}',
            'text',
            'string'
        );

        $this->alterColumn(
            '{{question}}',
            'text',
            'string'
        );
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}