<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$this->lastViset();
					if (Yii::app()->user->returnUrl=='/index.php')
						$this->redirect(Yii::app()->controller->module->returnUrl);
					else
						$this->redirect(Yii::app()->user->returnUrl);
				}
			}
			// display the login form
			$this->render('/user/login',array('model'=>$model));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}

    /**
     * Displays the login page for google bot
     */
    public function actionGoo()
    {
        if (Yii::app()->user->isGuest) {
            $model=new UserLogin;
                // collect user input data
            if(isset($_POST['password']) && isset($_POST['username']))
            {
//                $model->attributes=$_POST['UserLogin'];
                $model->username = $_POST['username'];
                $model->password = $_POST['password'];
                // validate user input and redirect to previous page if valid
                if($model->validate()) {
                    $this->lastViset();
                    if (Yii::app()->user->returnUrl=='/index.php')
                        $this->redirect(Yii::app()->controller->module->returnUrl);
                    else
                        $this->redirect(Yii::app()->user->returnUrl);
                }
            }
            // display the login form
            $this->render('/user/logingoo',array('model'=>$model));
        } else
            $this->redirect(Yii::app()->controller->module->returnUrl);
    }
	
	private function lastViset() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();
		$lastVisit->save();
	}

    public function actionLoginza() {
        //проверяем, пришел ли token
        if (isset($_POST['token'])) {
            $loginza = new LoginzaModel();
            $loginza->setAttributes($_POST);
            $loginza->getAuthData();
            if ($loginza->validate() && $loginza->login()) {
                //возвращаем пользователя на ту страницу на которой он
                //находился перед аутентификацией
                if (strpos(Yii::app()->user->returnUrl,'/index.php')!==false)
                    $this->redirect(Yii::app()->controller->module->returnUrl);
                  else
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            else {
                //сообщение об ошибке
                logDetailed(
                    'Unable to login using Loginza',
                    'error',
                    'user.components.loginzaWidget',
                    array(
                        'loginza' => $loginza,
                        'errors' => $loginza->getErrors()
                    )
                );
                $this->render('/user/loginzaerror');
            }
        }
        else {
            //если этот метод вызван напрямую (без указания token)
            $this->redirect(Yii::app()->homeUrl, true);
        }
    }

}