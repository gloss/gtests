<?php
class LoginzaModel extends CModel
{
    public $identity;
    public $provider;
    public $email;
    public $full_name;
    public $token;
    public $error_type;
    public $error_message;

    private $loginzaAuthUrl = 'http://loginza.ru/api/authinfo?token=';

    public function rules() {
        return array(
            array('identity,provider,token', 'required'),
            array('email', 'email'),
            array('identity,provider,email', 'length', 'max'=>255),
            array('full_name', 'length', 'max'=>255),
        );
    }

    public function attributeLabels() {
        return array(
            'identity'=>'Идентификатор сервиса аутентификации',
            'provider'=>'Сервис аутентификации',
            'email'=>'eMail',
            'full_name'=>'Имя',
        );
    }

    /**
     * Получение данных от сервиса Loginza.
     * Предварительно нужно установить $this->token
     * Например, так
     * $loginza = new LoginzaModel();
     * $loginza->setAttributes($_POST);
     */
    public function getAuthData() {
        //echo($this->loginzaAuthUrl.$this->token);die();
        //получаем данные от сервера Loginza
        $authData = json_decode(
            file_get_contents($this->loginzaAuthUrl.$this->token)
            ,true);

        if(array_key_exists('error_type', $authData) && $authData['error_type'] !== 0) {
            $message = 'Error getting information from Loginza.<br/>\n';
            $message .= "Error: " . $authData['error_message'] . " (" . $authData['error_type'] . ")<br/>\n";
            Yii::log($message, "error", "user.components.loginzaWidget");
            return false;
        }

        //устанавливаем атрибуты
        //если будут отсутствовать identity и provider, метод validate
        //выдаст ошибку
        $this->setAttributes($authData);
        //full_name находится внутри вложенного массива
        //echo '<pre>'; var_dump($authData); echo '</pre>'; die();
        $nickname = null;
        if(array_key_exists('nickname', $authData)) {
            $nickname = $authData['nickname'];
        }
        $this->full_name = (isset($authData['name']['full_name'])) ? $authData['name']['full_name'] : ((null != $nickname) ? $nickname : $authData['identity']);
        return true;
    }

    /**
     * Аутентификация посетителя.
     * @return boolean true - если посетитель аутентифицирован, false - в противном случае.
     */
    public function login() {
        $identity = new LoginzaUserIdentity();
        if ($identity->authenticate($this)) {
            $duration = 3600*24*30; // 30 days
            Yii::app()->user->login($identity,$duration);
            return true;
        }
        return false;
    }

    public function attributeNames() {
        return array(
            'identity'
        ,'provider'
        ,'email'
        ,'full_name'
        ,'token'
        ,'error_type'
        ,'error_message'
        );
    }
}
