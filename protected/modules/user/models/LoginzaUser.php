<?php
/**
 * The followings are the available model relations:
 * @property array $identities
 */
class LoginzaUser extends User
{

    public function relations() {
        $relations = parent::relations();
        $relations['identities'] = array(self::HAS_MANY, 'LoginzaUserIdentityModel', 'user_id');
    }


}
