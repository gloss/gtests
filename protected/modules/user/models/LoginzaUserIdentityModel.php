<?php

/**
 * This is the model class for table "{{user_identities}}".
 *
 * The followings are the available columns in table '{{user_identities}}':
 * @property integer $id
 * @property integer $user_id
 * @property string $provider
 * @property string $identity
 * @property string $email
 * @property string $full_name
 *
 * The followings are the available model relations:
 * @property User $user
 */
class LoginzaUserIdentityModel extends CActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LoginzaUserIdentityModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->getModule('user')->tableUserIdentities;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, provider, identity', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('provider, identity, email, full_name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, provider, identity, email, full_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'provider' => 'Provider',
			'identity' => 'Identity',
			'email' => 'Email',
			'full_name' => 'Full Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('provider',$this->provider,true);
		$criteria->compare('identity',$this->identity,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('full_name',$this->full_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @param string $condition
     * @param array $params
     * @return LoginzaUserIdentityModel
     */
    public function find($condition = '', $params = array())
    {
        return parent::find($condition, $params);
    }

    /**
     * @return LoginzaUserIdentityModel
     */
    public function with()
    {
        return parent::with();
    }


}