<?php

class m121212_102604_loginza_index_error extends CDbMigration
{
	public function up()
	{
		$this->dropIndex('user_provider', Yii::app()->getModule('user')->tableUserIdentities);
		$this->createIndex('user_provider', Yii::app()->getModule('user')->tableUserIdentities, 'provider', false);
	}

	public function down()
	{
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
