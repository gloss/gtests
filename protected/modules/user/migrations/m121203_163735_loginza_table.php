<?php
/**
 * Creates a table for storing Loginza IDs for the user
 */

class m121203_163735_loginza_table extends CDbMigration
{
    public function up()
    {
        if (!Yii::app()->getModule('user')) {
            echo "\n\nAdd to console.php :\n"
                . "'modules'=>array(\n"
                . "...\n"
                . "    'user'=>array(\n"
                . "        ... # copy settings from main config\n"
                . "    ),\n"
                . "...\n"
                . "),\n"
                . "\n";
            return false;
        }
        Yii::import('user.models.User');
        $this->createTable(
            Yii::app()->getModule('user')->tableUserIdentities,
            array(
                'id' => 'pk',
                'user_id' => 'int NOT NULL',
                'provider' => 'varchar(255) NOT NULL',
                'identity' => 'varchar(255) NOT NULL',
                'email' => 'varchar(255)',
                'full_name' => 'varchar(255)',
            )
        );
        $this->addForeignKey('user_identities_user_id', Yii::app()->getModule('user')->tableUserIdentities, 'user_id', Yii::app()->getModule('user')->tableUsers, 'id', 'CASCADE', 'CASCADE');
        $this->dropIndex('user_email', Yii::app()->getModule('user')->tableUsers);
        $this->createIndex('user_email', Yii::app()->getModule('user')->tableUsers, 'email', false);
        $this->createIndex('user_identity', Yii::app()->getModule('user')->tableUserIdentities, 'identity', true);
        $this->createIndex('user_provider', Yii::app()->getModule('user')->tableUserIdentities, 'provider', true);
    }

    public function safeDown()
    {
        $this->dropTable(Yii::app()->getModule('user')->tableUserIdentities);
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}