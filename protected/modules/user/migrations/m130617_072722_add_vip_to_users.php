<?php

//$ yiic migrate create --migrationPath=user.migrations add_vip_to_users

class m130617_072722_add_vip_to_users extends CDbMigration
{
	public function up()
	{
        $this->addColumn(
            '{{users}}',
            'vip',
            'int(1) NOT NULL DEFAULT 0'
        );
	}

	public function down()
	{
        $this->dropColumn('{{users}}', 'vip');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}