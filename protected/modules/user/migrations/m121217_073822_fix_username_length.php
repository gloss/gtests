<?php

class m121217_073822_fix_username_length extends CDbMigration
{
	public function up()
	{
		$this->alterColumn(
			'{{users}}',
			'username',
			'varchar(255)'
		);
	}

	public function down()
	{
		echo "m121217_073822_fix_username_length does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
