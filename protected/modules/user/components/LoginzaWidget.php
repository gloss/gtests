<?php
class LoginzaWidget extends CWidget {
    //параметры по-умолчанию
    private $params = array(
        'widget_url'=>'http://s1.loginza.ru/js/widget.js',//'https://s3-eu-west-1.amazonaws.com/s1.loginza.ru/js/widget.js',
        'token_url'=>'https://loginza.ru/api/widget?token_url=',
        'return_url'=>'',
        'link_anchor'=>'Войти с помощью Loginza',
        'logout_url'=>'',
        'inline' => false,
        'css_class'=>'loginza',
        'providers_set'=>array('vkontakte', 'facebook', 'twitter', 'loginza'
        , 'myopenid', 'webmoney', 'rambler', 'flickr', 'lastfm', 'openid'
        , 'mailru', 'verisign', 'aol', 'steam', 'google', 'yandex'
        , 'mailruapi'),
    );

    /**
     * Этот метод подключает JS скрипт и загружает представление
     */
    public function run() {
        $url = $this->params['token_url'].Yii::app()->createAbsoluteUrl($this->params['return_url']).'&providers_set='.implode(',',$this->params['providers_set']);
        //подключаем JS скрипт
        Yii::app()->clientScript->registerScriptFile(
            $url//$this->params['widget_url']
            , CClientScript::POS_END);
	if(!$this->params['inline']) {
		$this->render('loginzaWidget', $this->params);
	} else {
		$this->render('loginzaWidgetInline', $this->params);
	}
    }

    /**
     * Установка параметров
     * @param array $params массив с параметрами
     */
    public function setParams($params) {
        $this->params = array_merge($this->params, $params);
    }
}
