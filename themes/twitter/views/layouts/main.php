<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <meta name="google-site-verification" content="SY6D5kk0MwrvkH1nquE8uO9c-SJnRPdsUGs_5PiFjNY" />
    <meta name='yandex-verification' content='4b66404d54a5bf8b' />
    <link rel="shortcut icon" href="http://faviconist.com/icons/20e90d1dc37692cbd385e45b50a4d415/favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<?php // http://stackoverflow.com/questions/9386429/simple-bootstrap-page-is-not-responsive-on-the-iphone ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <?php /*Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl . "/js/jquery.min.js", CClientScript::POS_END
);*/
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/main.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/bootstrap.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/bootstrap-responsive.min.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/main.css');
    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl . "/js/bootstrap.min.js", CClientScript::POS_END
    );
    Yii::app()->clientScript->registerScriptFile(
        Yii::app()->theme->baseUrl . "/js/main.js", CClientScript::POS_END
    ); ?>
    <script>
        document.documentElement.className += ' js';
    </script>

    <link rel="image_src" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.jpg" />

    <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-startup-image" href="/startup.png">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php
    $userHide = Yii::app()->getModule('user')->shouldHideAds();
    $shouldHideAds = (property_exists($this, 'hideAds') && $this->hideAds === true) ||
        Yii::app()->getModule('user')->shouldHideAds();
    ?>
</head>

<body>
<div class="hidden"><b>Если Вы видите это сообщение, что-то пошло не так.
 Перезагрузите, пожалуйста, страницу, и все должно заработать :)</b><br/><br/><br/></div>
<?php include_once('ya_metrika.php'); ?>
<?php include_once('ganalytics.php'); ?>

<script type="text/javascript">
    window.google_analytics_uacct = "UA-24182259-4";
</script>

<div class="navbar navbar-fixed-top">
    <!-- some comment -->
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand"
			   href="<?php echo Yii::app()->getHomeUrl(); ?>"
			   title="<?php echo CHtml::encode(Yii::app()->name); ?>">
				<?php echo CHtml::encode(Yii::app()->name); ?>
                <span class="badge-beta">Beta</span>
			</a>

            <div class="nav-collapse">
                <?php $this->widget(
                'zii.widgets.CMenu', array(
                                          //   $this->widget('ext.CDropDownMenu.CDropDownMenu',array(
                                          //            'id'=>'myMenu',
                                          'htmlOptions'     => array('class' => 'nav'),
                                          'activateItems'   => true,
                                          'activateParents' => true,
                                          'activeCssClass'  => 'active',
                                          'items'           => array(
											  array('label'  => 'Описание', 'url'=> '/',
                                                  'active' => Yii::app()->controller->getId() == 'site',
                                                  'visible'=> true || Yii::app()->user->isGuest),
											  array('label'  => 'Тесты 1С', 'url'=> array('/category/index'),
                                                  'active' => Yii::app()->controller->getId() == 'category'
                                                        || Yii::app()->controller->getId() == 'test',
													'visible'=> true || !Yii::app()->user->isGuest),
											  array('label'  => 'Блог', 'url'=> 'http://1c-test.ru/blog/',
													'visible'=> true || Yii::app()->user->isGuest),
                                          ),
                                     )
            ); ?>
                <p class="navbar-text pull-right">
                    <?php
                    $this->renderDynamic(array("SiteController", "getUserLink"));
                    ?>
                    <!--                    --><?php //_e('Logged in as') ?><!-- <a href="#">username</a>-->
                </p>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<div class="container-fluid" id="content-wrapper">
    <div class="row-fluid" id="content-fluid">
        <?php
        $url = Yii::app()->request->requestUri;
        $url = substr($url, 0, strpos($url, '?'));
        $messages = Yii::app()->params['sitewideMessages'];
        if(!empty($messages) && is_array($messages)) {
            foreach($messages as $name=>$message) {
                $dismissed = isset($_COOKIE['hide_' . $name]);
                $hideGuest = !isset($message['hideGuests']) || $message['hideGuests'] === true;
                $skipUrl = false;
                if(isset($message['excludeUrls']) && is_array($message['excludeUrls'])) {
                    foreach($message['excludeUrls'] as $excludeUrl) {
                        if($excludeUrl === $url) {
                            $skipUrl = true;
                            break;
                        }
                    }
                }
                if(!isset($message['text'])
                    || (isset($message['active']) && $message['active'] === false)
                    || $dismissed
                    || ($hideGuest && Yii::app()->user->isGuest)
                    || $skipUrl) {
                    unset($messages[$name]);
                }
            }
        } else {
            $messages = array();
        }
        if(count($messages) > 0) :
            Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/site-warnings.css');
            Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/site-warnings.js');
        ?>
            <?php foreach($messages as $name=>$message) : ?>
                <? $class = isset($message['class']) ? $message['class'] : 'alert-info'; ?>
                <? $dismissable = isset($message['dismissable']) ? $message['dismissable'] : true; ?>
                <? $dismissDays = isset($message['dismissDays']) ? $message['dismissDays'] : 1; ?>
                <? if($dismissable) $class .= " alert-dismissable"; ?>
                <div class="alert alert-header <?= $class; ?> " id="alert-<?=$name;?>" data-name="<?=$name;?>">
                    <? if($dismissable) : ?>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                                data-name="<?=$name;?>" data-dismissdays="<?=$dismissDays;?>">&times;</button>
                    <? endif; ?>
                    <?= $message['text']; ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if (isset($this->breadcrumbs)): ?>
            <?php $this->widget(
            'zii.widgets.CBreadcrumbs', array(
                                             'htmlOptions' => array('class' => "breadcrumb"),
                                             'links'    => $this->breadcrumbs,
                                             'homeLink' => CHtml::link('Главная', Yii::app()->homeUrl),
                                        )
        ); ?><!-- breadcrumbs -->
        <?php endif?>
    <?php if(!$shouldHideAds) : ?>
	<div id='adsense'>
        <script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

        <ins class="adsbygoogle"
             id="adsbygoogle-1c-test"
             style="display:inline-block;width:468px;height:60px"
             data-ad-client="ca-pub-9068766447468997"
             data-ad-slot="3522642861"></ins>
        <script>
            var insElement = document.getElementById('adsbygoogle-1c-test');
            if (window.innerWidth >= 800) {
                insElement.setAttribute("data-ad-slot", "5697380063");
                insElement.style.width="728px";
                insElement.style.height="90px";
            } else if (window.innerWidth < 400) {
                insElement.setAttribute("data-ad-slot", "9569176465");
                insElement.style.width="320px";
                insElement.style.height="50px";
            }
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
	</div>
    <?php endif; ?>

        <?php echo $content; ?>

        <?php if(!$shouldHideAds) : ?>
        <!-- Yandex donation -->
        <!--iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/donate.xml?uid=41001270965767&amp;default-sum=200&amp;targets=%D0%A0%D0%B0%D0%B7%D0%B2%D0%B8%D1%82%D0%B8%D0%B5+%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0&amp;project-name=%D0%9F%D0%BE%D0%B4%D0%B3%D0%BE%D1%82%D0%BE%D0%B2%D0%BA%D0%B0+%D0%BA+%D1%82%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8E+1%D0%A1%3A%D0%9F%D1%80%D0%BE%D1%84%D0%B5%D1%81%D1%81%D0%B8%D0%BE%D0%BD%D0%B0%D0%BB&amp;project-site=http%3A%2F%2F1c-test.ru&amp;button-text=05&amp;hint=&amp;mail=on" width="320" height="105"></iframe-->
        <iframe class="support-button" src="https://money.yandex.ru/embed/small.xml?uid=41001270965767&amp;button-text=06&amp;button-size=m&amp;button-color=orange&amp;targets=%D0%A0%D0%B0%D0%B7%D0%B2%D0%B8%D1%82%D0%B8%D0%B5+%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0+%22%D0%9F%D0%BE%D0%B4%D0%B3%D0%BE%D1%82%D0%BE%D0%B2%D0%BA%D0%B0+%D0%BA+%D1%82%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8E+1%D0%A1%3A%D0%9F%D1%80%D0%BE%D1%84%D0%B5%D1%81%D1%81%D0%B8%D0%BE%D0%BD%D0%B0%D0%BB%22&amp;default-sum=200&amp;mail=on" width="210" height="42"></iframe>
        <!-- Yandex donation -->
        <?php endif; ?>

        <div class="clear"></div>

        <div id="footer">
            <span class="nowrap socials">
                <a href="http://vk.com/1c_prof" class="social-icon vk-icon" target="_blank" rel="nofollow"></a>
                <a href="https://twitter.com/1c_test" class="social-icon twit-icon" target="_blank" rel="nofollow"></a>
            </span>
            <span class="nowrap copyright">&copy; 2012-<?php $date = getdate(); echo $date['year']; ?> <a href="<?php echo Yii::app()->getHomeUrl(); ?>">Тестирование
                1С:Профессионал</a></span>
			<!--<span class="nowrap"><?php echo Yii::powered(); ?></span>-->

        </div>
        <!-- footer -->
    </div>
</div>
<!-- page -->

<script type="text/javascript">
    var reformalOptions = {
        project_id: 83442,
        project_host: "1c-test.reformal.ru",
        tab_orientation: "left",
        tab_indent: "50%",
        tab_bg_color: "#f9c700",
        tab_border_color: "#FFFFFF",
        tab_image_url: "http://tab.reformal.ru/0JXRgdGC0Ywg0LjQtNC10Lg%252F/FFFFFF/c2c31794c81c9f99fd1245c54bb3fd65/left/1/tab.png",
        tab_border_width: 2
    };

    (function() {
        var script = document.createElement('script');
        script.type = 'text/javascript'; script.async = true;
        script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'media.reformal.ru/widgets/v3/reformal.js';
        document.getElementsByTagName('head')[0].appendChild(script);
    })();
</script><noscript><a href="http://reformal.ru" rel="nofollow"><img src="http://media.reformal.ru/reformal.png" alt="Обсуждение проекта 1c-test.ru" /></a><a href="http://1c-test.reformal.ru" rel="nofollow">Есть идеи?</a></noscript>

</body>
</html>
