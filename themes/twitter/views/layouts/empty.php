<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
<?php // http://stackoverflow.com/questions/9386429/simple-bootstrap-page-is-not-responsive-on-the-iphone ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.scss"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"/>

    <link rel="stylesheet" type="text/css"
    href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css"
    href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css"/>
    <?php Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl . "/js/jquery.min.js", CClientScript::POS_END
); ?>
    <?php Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl . "/js/bootstrap.min.js", CClientScript::POS_END
    ); ?>
    <script>
        document.documentElement.className += ' js';
    </script>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="#"><?php echo CHtml::encode(Yii::app()->name); ?></a>

            <div class="nav-collapse">
                <?php $this->widget(
                'zii.widgets.CMenu', array(
                                          //   $this->widget('ext.CDropDownMenu.CDropDownMenu',array(
                                          //            'id'=>'myMenu',
                                          'htmlOptions'     => array('class' => 'nav'),
                                          'activateItems'   => true,
                                          'activateParents' => true,
                                          'activeCssClass'  => 'active',
                                          'items'           => array(
                                              array('label'  => 'Тесты', 'url'=> array('/category/index'),
                                                    'visible'=> !Yii::app()->user->isGuest),
                                              array('label'  => 'Описание', 'url'=> array('/site/index'),
                                                    'visible'=> Yii::app()->user->isGuest),
                                          ),
                                     )
            ); ?>
                <p class="navbar-text pull-right">
                    <?php
                    if (Yii::app()->user->isGuest) {
                        echo CHtml::link(
                            "Зарегистрироваться",
                            Yii::app()->getModule('user')->registrationUrl, array('class' => 'btn btn-primary')
                        ) . ' или '
                            . CHtml::link(
                                "Войти", Yii::app()->getModule('user')->loginUrl
                            );
                    } else {
                        echo 'Вы вошли как ' . ' ' . Yii::app()->user->name . '. ' . CHtml::link(
                            "Выйти", Yii::app()->getModule('user')->logoutUrl
                        );
                    }
                    ?>
                    <!--                    --><?php //_e('Logged in as') ?><!-- <a href="#">username</a>-->
                </p>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>

<div class="container-fluid" id="content-wrapper">
    <div class="row-fluid" id="content-fluid">

        <?php if (isset($this->breadcrumbs)): ?>
            <?php $this->widget(
            'zii.widgets.CBreadcrumbs', array(
                                             'htmlOptions' => array('class' => "breadcrumb"),
                                             'links'    => $this->breadcrumbs,
                                             'homeLink' => CHtml::link('Главная', Yii::app()->homeUrl),
                                        )
        ); ?><!-- breadcrumbs -->
        <?php endif?>

        <?php echo $content; ?>

        <div class="clear"></div>

        <div id="footer">
            <span class="nowrap">
                <a href="http://vk.com/1c_prof" class="social-icon vk-icon" target="_blank"></a>
                <a href="https://twitter.com/1c_test" class="social-icon twit-icon" target="_blank"></a>
            </span>
            <span class="nowrap">&copy; 2012 <a href="<?php echo Yii::app()->getHomeUrl(); ?>">Тестирование
                1С:Профессионал</a></span>
            <?php echo Yii::powered(); ?>
        </div>
        <!-- footer -->
    </div>
</div>
<!-- page -->

</body>
</html>
