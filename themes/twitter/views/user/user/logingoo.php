<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
$this->breadcrumbs=array(
    UserModule::t("Login"),
);
?>

<h1><?php echo UserModule::t("Login"); ?></h1>

<p>
    <?php // echo UserModule::t("Please fill out the following form with your login credentials:"); ?>
    Возможно, мы знакомы! Представьтесь, пожалуйста, чтобы мы смогли соханить Ваши результаты прохождения теста.
</p>

<div id="login-form">

        <h3>Войдите используя Ваш аккаунт:</h3>

        <?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

        <div class="success">
            <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
        </div>

        <?php endif; ?>

        <div class="form">
            <?php echo CHtml::beginForm(); ?>

            <?php echo CHtml::errorSummary($model); ?>

            <div class="row">
                <!--<?php echo CHtml::activeLabelEx($model,'username'); ?>
                <?php echo CHtml::activeTextField($model,'username') ?>-->
                <label for="username">Username</label>
                <input type="text" id="username" name="username"/>
            </div>

            <div class="row">
                <!--<?php echo CHtml::activeLabelEx($model,'password'); ?>
                <?php echo CHtml::activePasswordField($model,'password') ?>-->
                <label for="password">Password</label>
                <input type="password" id="password" name="password"/>
            </div>

            <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

            <div class="row">
                <p class="hint">
                    <!--		--><?php //echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?><!-- | --><?php //echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
                    <?php echo CHtml::link("Давайте познакомимся! (регистрация)",Yii::app()->getModule('user')->registrationUrl); ?> | <?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
                </p>
            </div>

            <div class="row rememberMe">
                <?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
                <?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
            </div>

            <div class="row submit">
                <?php echo CHtml::submitButton(UserModule::t("Login")); ?>
            </div>

            <?php echo CHtml::endForm(); ?>
        </div><!-- form -->
</div>