<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
$this->breadcrumbs=array(
	UserModule::t("Login"),
);
?>

<h1><?php echo UserModule::t("Login"); ?></h1>

<p>
	<?php // echo UserModule::t("Please fill out the following form with your login credentials:"); ?>
	Возможно, мы знакомы! Представьтесь, пожалуйста, чтобы мы смогли соханить Ваши результаты прохождения теста.
</p>

<div id="login-form">

	<div id="login-middle" class="login-div login-separator">
		<span class="login-vertical-line"></span>
		<span>или</span>
		<span class="login-vertical-line"></span>
	</div>

	<div id="login-right" class="login-div">

		<h3>Войдите используя Ваш аккаунт:</h3>

		<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

		<div class="success">
			<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
		</div>

		<?php endif; ?>

		<div class="form">
		<?php echo CHtml::beginForm(); ?>
			
			<?php echo CHtml::errorSummary($model); ?>
			
			<div class="row">
				<?php echo CHtml::activeLabelEx($model,'username'); ?>
				<?php echo CHtml::activeTextField($model,'username') ?>
			</div>
			
			<div class="row">
				<?php echo CHtml::activeLabelEx($model,'password'); ?>
				<?php echo CHtml::activePasswordField($model,'password') ?>
			</div>

			<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
			
<div class="row">
	<div class="alert">Внимание! Регистрация с 22.05.2014 осуществляется на новой версии сервиса. Если Ваши логин и пароль не подходят, попробуйте <a href="http://app.1c-test.ru/users/login">войти здесь</a>.</div>
</div>
			<div class="row">
				<p class="hint">
		<!--		--><?php //echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?><!-- | --><?php //echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
				<?php echo CHtml::link("Давайте познакомимся! (регистрация)",Yii::app()->getModule('user')->registrationUrl); ?> | <?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
				</p>
			</div>
			
			<div class="row rememberMe">
				<?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
				<?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
			</div>

			<div class="row submit">
				<?php echo CHtml::submitButton(UserModule::t("Login")); ?>
			</div>
			
		<?php echo CHtml::endForm(); ?>
		</div><!-- form -->


		<?php
		$form = new CForm(array(
		    'elements'=>array(
		        'username'=>array(
		            'type'=>'text',
		            'maxlength'=>32,
		        ),
		        'password'=>array(
		            'type'=>'password',
		            'maxlength'=>32,
		        ),
		        'rememberMe'=>array(
		            'type'=>'checkbox',
		        )
		    ),

		    'buttons'=>array(
		        'login'=>array(
		            'type'=>'submit',
		            'label'=>'Login',
		        ),
		    ),
		), $model);
		?>

	</div>

	<div id="login-middle-horizontal" class="login-div login-separator nowrap">
		<span class="login-horizontal-line"></span>
		<span>или</span>
		<span class="login-horizontal-line"></span>
	</div>


	<div id="login-left" class="login-div">
		<h3>Вы можете войти как:</h3>

		<?php if(Yii::app()->getModule('user')->useLoginza) {
		    $this->widget('user.components.LoginzaWidget', array(
		        'params' => array(
		            'return_url' => 'user/login/loginza',
		            'logout_url' => 'user/logout',
		            'providers_set' => array('google', 'vkontakte', 'facebook', 'twitter', 'yandex'),
			    'inline' => true,
		        )
		    ));
		} ?>

		<p>Осуществление входа на ресурс означает Ваше полное согласие с условиями использования сервиса (<?php echo CHtml::link("пользовательским соглашением", array('/site/agreement')); ?>).</p>

	</div>
</div>
