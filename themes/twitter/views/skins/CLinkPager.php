<?php
/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 * @date   07.09.12
 */
return array(
	'default' => array(
		'cssFile'              => false,
		'htmlOptions'          => array('class' => "pagination center yiiPager"),
		'selectedPageCssClass' => 'active selected',
		'hiddenPageCssClass'   => 'disabled',
		'firstPageLabel'       => '<<',
		'prevPageLabel'        => '<',
		'nextPageLabel'        => '>',
		'lastPageLabel'        => '>>',
		'header'               => '',
	),
);
?>