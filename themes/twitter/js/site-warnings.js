function setCookie(c_name,value,exdays)
{
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString()) + ";path=/";
    document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name)
{
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1)
    {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1)
    {
        c_value = null;
    }
    else
    {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1)
        {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}

$(document).ready(function(){
    $('.alert-header > button.close').on('click', function(){
        var name = $(this).data('name'),
            days = $(this).data('dismissdays');
        setCookie('hide_' + name, true, days);
    });

    // For the HTML-cached front page
    $('.alert-header').each(function() {
        var $this = $(this),
            name = $this.data('name');
        console.log(getCookie(name));
        if(getCookie(name) !== null) {
            $this.remove();
        }
    });
});
