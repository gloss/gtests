$(document).ready(function(){
    var $spoiler = $('.spoiler');
    var $children = $spoiler.children('.spoilerCont');
    $spoiler.children('a').on('click', function(e) {
        e.preventDefault();
        if($spoiler.hasClass('open')) {
            $spoiler.removeClass('open');
            $(this).attr('title', 'Показать подсказку к вопросу');
            $children.slideUp(200);
        } else {
            $spoiler.addClass('open');
            $children.slideDown(300);
            $(this).attr('title', 'Скрыть подсказку к вопросу');
        }
    });
});