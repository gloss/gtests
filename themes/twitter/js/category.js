$(document).ready(function(){
    $(".additional-menu .switcher").click(function(){
        var $switcher = $(this),
            $menu = $switcher.
                parent().
                find(".content").
                first(),
            oldName = $switcher.html();
        $switcher.html($switcher.data("hiddentext"));
        $switcher.data("hiddentext", oldName);
        $menu.toggle();
    });
});