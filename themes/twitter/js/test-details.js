$(document).ready(function() {
    $('.explain').on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            $linkedTd = $('#' + $this.data('id'));
        console.log($linkedTd);
        $this.toggleClass('open');
        $linkedTd.toggle();
    });
});