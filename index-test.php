<?php
/**
 * This is the bootstrap file for test application.
 * This file should be removed when the application is deployed for production.
 */

// remove the following line when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);

// change the following paths if necessary
$yii=dirname(__FILE__).'/protected/vendor/yiisoft/yii/framewor/yii.php';
$config=dirname(__FILE__).'/protected/config/test.php';

require_once($yii);
Yii::createWebApplication($config)->run();
