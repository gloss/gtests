/**
 * @author Stanislav Sidelnikov <sidelnikov.stanislav@gmail.com>
 * @date 28.09.12
 */
var $circleLinks = $('.circle-item > a'),
    $circles = $('.circle-item'),
    $mainContent = $('.main-content'),
    $loadingDiv = $('#loading');

$circles.css('opacity', '0.7');
$circleLinks.hover(
    function() {
        var $parent = $(this).parent('li');
        console.log($parent);
        $parent.stop().animate({opacity: 1}, 'fast');
    },
    function() {
        var $parent = $(this).parent('li');
        $parent.stop().animate({opacity: 0.7}, 700);
    }
);

$circleLinks.click(function(e) {
    e.preventDefault();
    var url = $(this).attr('href');
    $.ajax(url, {
        'beforeSend': function() {
            $loadingDiv.show();
        }
    }).
        always(function(){
            $loadingDiv.hide();
        }).
        success(function(data){
        $mainContent.html(data);
    });
});